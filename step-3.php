<?php
include "lib/helper.php";

if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}

session_start();

if(!isset($_SESSION['client']['prebook'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['kode'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['nama'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['email'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['idusers'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['step'])){
  move("?page=index");
}

if(isset($_SESSION['client']['step'])){
  if($_SESSION['client']['step'] == 1){
    move("?page=step2");
  } else if($_SESSION['client']['step'] == 3){
    move("?page=step4");
  }
} else{
  move("?page=index");
}

if(!isset($_SESSION['client']['idusers'])){
  header("Location: ./");
  die();
}

require "vendor/autoload.php";
use dbase\datafunction;

$z = new datafunction();

$kode = $_SESSION['client']['kode'];

$event = (object) $z->getEventByKode($kode);
?>


<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="package/font-awesome/css/font-awesome.min.css">

  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="canvas/js/fabric.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="package/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <title>Tamugo!</title>
</head>
<body>
  <div class="header">
    <nav class="navbar navbar-light bg-light shadow-sm">
      <a class="navbar-brand"><img src="img/icon.png" width="100px"></a>
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0 base" type="submit">Search</button>
      </form>
    </nav>
  </div>
  <div class="shadow-sm w-80 center">
    <div class="row contain">
      <div class="col-lg-12">
        <div class="row bs-wizard" style="border-bottom:0;">
          <div class="col-xs-3 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum">Langkah 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Konfirmasi Kehadiran</div>
          </div>

          <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 2</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Pilih Kursi / Tempat Duduk Anda</div>
          </div>

          <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
            <div class="text-center bs-wizard-stepnum"><b>Langkah 3</b></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center textactive">Cek Ulang Data</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Step 4</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cetak Kode QR</div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
          <div class="col-lg-7">
            <h4>Detail Event</h4>
            <div class="row" style="margin-top: 2rem;">
              <div class="col-lg-12">
                <!-- <h5>Kode Undangan</h5> -->
                <center>
                  <div class="col-lg-7 code">
                    <h5><?php echo $_SESSION['client']['kode']; ?></h5>
                  </div>
                </center>
              </div>
            </div>

            <div class="row" style="margin-top: 2rem;">
              <div class="col-md-2 col-xs-12">
                <h6>Nama</h6>
              </div>
              <div class="col-md-1 col-xs-12">
                <h6>:</h6>
              </div>
              <div class="col-md-9 col-xs-12">
                <?php echo $event->nama_event; ?>
              </div>
            </div>
            <div class="row" style="margin-top: 8px;">
              <div class="col-md-2 col-xs-12">
                <h6>Alamat</h6>
              </div>
              <div class="col-md-1 col-xs-12">
                <h6>:</h6>
              </div>
              <div class="col-md-9 col-xs-12">
                <?php echo $event->alamat_event; ?>
              </div>
            </div>
            <div class="row" style="margin-top: 8px;">
              <div class="col-md-2 col-xs-12">
                <h6>Contact</h6>
              </div>
              <div class="col-md-1 col-xs-12">
                <h6>:</h6>
              </div>
              <div class="col-md-9 col-xs-12">
                <?php echo $event->contact_event; ?>
              </div>
            </div>
            <div class="row" style="margin-top: 8px;">
              <div class="col-md-2 col-xs-12">
                <h6>Date</h6>
              </div>
              <div class="col-md-1 col-xs-12">
                <h6>:</h6>
              </div>
              <div class="col-md-9 col-xs-12">
                <?php echo $event->date; ?>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="col-lg-12">
              <h4>Detail Kursi</h4>
              <div class="row" style="margin-top: 2rem;" id="rowstat">
                <div class="col-lg-12 text-center">
                  <canvas id="canvas" width="300" height="300"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
            <div class="col-lg-12">
              <a href="?page=cancel">
                <button type="button" name="button" class="btnck float-left">Batal</button>
              </a>

              <button type="submit" name="button" class="btnext float-right" id="btnnext" href="?page=confirm">Next</button>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  $("document").ready(function(){
    var canvas = new fabric.Canvas('canvas');
    var cs = $("#canvas");

    cs.css("border","1px solid #ccc");

    function resizeCanvas() {
      var bwidth = cs.parent().parent().css("width");
      canvas.setWidth(parseInt(bwidth)+5);
      canvas.renderAll();
    }
    window.addEventListener('resize', resizeCanvas, false);
    resizeCanvas();

    function genid(){
      var unix = moment().format("x");
      var date = moment().format("DDMMYY");
      var rand = chance.integer({min:1000,max:9999});
      var final = "T"+date+rand;
      return final;
    }

    function exportJSON(json){
      if(json.objects.length > 0){
        var tmp = [];

        for(object in json.objects){
          if(json.objects[object].type != "line"){
            json.objects[object].lockRotation  = true;

            json.objects[object].lockScalingY  = true;
            json.objects[object].lockScalingX  = true;

            json.objects[object].lockMovementX = true;
            json.objects[object].lockMovementY = true;

            json.objects[object].hasControls   = false;
            json.objects[object].hasBorders    = false;

            json.objects[object].hoverCursor   = "pointer";

            tmp.push(json.objects[object]);
          }
        }
      }
      var data = {};
      data["version"] = "2.3.4";
      data["objects"] = tmp;

      return data;
    }

    function zoomIt(factor,json) {
      // canvas.setHeight(canvas.getHeight() * factor);
      // canvas.setWidth(canvas.getWidth() * factor);

      // if (canvas.backgroundImage) {
      //   // Need to scale background images as well
      //   var bi = canvas.backgroundImage;
      //   bi.width = bi.width * factor; bi.height = bi.height * factor;
      // }

      canvas.loadFromJSON(json, function() {
        canvas.renderAll();
      });

      var objects = canvas.getObjects();
      //var objects = json.objects;
      <?php
      $i = isset($_SESSION['client']['seat']) ? $_SESSION['client']['seat'] : "null";
      ?>
      for (var i in objects) {
        if(objects[i].id_seat == <?php echo $i; ?>){
          objects[i].fill = json.active;
        }

        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].left;
        var top = objects[i].top;

        // var width = objects[i].width;
        // var height = objects[i].height;

        var tempScaleX = scaleX * factor;
        var tempScaleY = scaleY * factor;
        var tempLeft = left * factor;
        var tempTop = top * factor;

        // var tempWidth = width * factor;
        // var tempHeight = height * factor;

        objects[i].scaleX = tempScaleX;
        objects[i].scaleY = tempScaleY;
        objects[i].left = tempLeft-20;
        objects[i].top = tempTop;

        objects[i].setCoords();
      }

      json = exportJSON(canvas.toJSON());
      // console.log(JSON.stringify(json));

      canvas.clear();
      canvas.loadFromJSON(json, function() {
        canvas.backgroundColor = "#dedede";
        canvas.renderAll();
        canvas.selection = false;
      });
    }

    $.ajax({
      url: "API/front/canvas/<?php echo $_SESSION['client']['kode']; ?>",
      type: "GET",
      success:function(res){
        zoomIt(0.6,res);
      },
      error:function(x,y,z){
        console.log(x,y,z);
      }
    });
    $("#btnnext").click(function(e){
      e.preventDefault();
      var link = $(this).attr("href");

      swal({
        title: 'Konfirmasi',
        text: "Apakah anda ingin mengkonfirmasi Event?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, konfirmasi!',
        cancelButtonText: 'Tidak'
      }).then((result) => {
        if (result.value) {
          swal(
            'Sukses!',
            'Kursi anda sudah dikonfirmasi.',
            'success'
          )
          setTimeout(function(){
            window.location.href = link;
          },1000);
        }
      })
      return false;
    });
  });
</script>

</body>
</html>
