<?php
if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}
//require "pages/page1.php";
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="https://unpkg.com/ionicons@4.2.4/dist/ionicons.js"></script>
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript">
    $("document").ready(function() {
      $('.cinema-seats .seat').click(function() {
        $(this).toggleClass('active');
      });

      var clk = 0;
      var cnt = 0;
      var ido = "";
      $('[data-toggle="tooltip"]').tooltip();

      $("#frmchair").submit(function(e){
        e.preventDefault();

        var z = $("#frmchair").serialize();
        console.log(z);
      });

      $("td").click(function(){
        var use = $(this).attr("class");

        if(cnt == 0){
          if(use != 'use'){
            $(this).css("background","var(--based)");
            ido = $(this).attr("id");
            cnt = 1;
          }
        } else{
          if(use != 'use'){
            $("td[id="+ido+"]").css("background","#eee");
            $(this).css("background","var(--based)");
            ido = $(this).attr("id");
            cnt++;
          }

          console.log(ido);

        }
        console.log(cnt);
        $(this).find('input').prop("checked",true);
      });

      $("input[type=radioz]").click(function(){
        console.log($(this).attr("id"));
        $(this).parent().css("background","var(--base)");
        //$(this).css("background","#eee");
      });
      $("input[type=reset]").click(function(){
        console.log("reset");
        $("td:not([class=use])").css("background","#eee");
      });

    });
  </script>

  <title>Tamugo!</title>
</head>
<body>
  <div class="header">
    <nav class="navbar navbar-light bg-light shadow-sm">
      <a class="navbar-brand"><img src="img/icon.png" width="100px"></a>
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0 base" type="submit">Search</button>
      </form>
    </nav>
  </div>
  <div class="shadow-sm w-80 center">
    <div class="row contain">
      <div class="col-lg-12">
        <div class="row bs-wizard" style="border-bottom:0;">

          <div class="col-xs-3 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum">Langkah 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Konfirmasi Kehadiran</div>
          </div>

          <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
            <div class="text-center bs-wizard-stepnum"><b>Langkah 2</b></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center textactive">Pilih Kursi / Tempat Duduk Anda</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 3</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cek Ulang Data</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Step 4</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cetak Kode QR</div>
          </div>

        </div>
      </div>
      <div class="col-lg-12">
        <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
          <div class="col-lg-12">
            <h4>Pilih Tempat Duduk</h4>
            <!--
            <div class="table-responsive">
              <table class="table table-tamugo">
                <tbody>
                  <form action="" id="frmchair">

                    <?php
                    /*
                    $data = 8;
                    $row = ceil(30/$data);

                    for($z=0;$z<$row;$z++){
                      echo "<tr>";
                      for($n=0;$n<$data;$n++){
                        ?>

                        <td id="open-<?php echo $n; ?>"><input type="radio" name="chr" id="chr-<?php echo $n; ?>" value="<?php echo $n; ?>"></td>

                        <?php
                      }
                      echo "</tr>";
                    }
                    */
                    ?>

                    <?php
                    $data = 22;
                    $cell = 8;

                    $row1 = floor($data/$cell);
                    $sisa = $data % $cell;

                    for($z=0;$z<$data;$z++){
                      if(!($z % $cell)) {
                        if($z > 0) {
                          //echo "\r\n</tr>\r\n";
                        }
                        //echo "<tr>\r\n";
                      }
                      //echo '<td id="open-'.$z.'"><input type="radio" name="chr" id="chr-'.$z.'" value="'.$z.'">X</td>'."\r\n";
                    }
                    ?>

                    <tr>
                      <td id="open-0" class="use" data-toggle="tooltip" data-placement="top" title="Telah Dipesan"><input type="radio" name="chr" id="chr-0" value="0" data-toggle="tooltip" disabled>X</td>
                      <td id="open-1"><input type="radio" name="chr" id="chr-1" value="1">X</td>
                      <td id="open-2"><input type="radio" name="chr" id="chr-2" value="2">X</td>
                      <td id="open-3"><input type="radio" name="chr" id="chr-3" value="3">X</td>
                      <td id="open-4" class="use" data-toggle="tooltip" data-placement="top" title="Telah Dipesan"><input type="radio" name="chr" id="chr-4" value="4">X</td>
                      <td id="open-5" class="use" data-toggle="tooltip" data-placement="top" title="Telah Dipesan"><input type="radio" name="chr" id="chr-5" value="5">X</td>
                      <td id="open-6"><input type="radio" name="chr" id="chr-6" value="6">X</td>
                      <td id="open-7" class="use" data-toggle="tooltip" data-placement="top" title="Telah Dipesan"><input type="radio" name="chr" id="chr-7" value="7" disabled>X</td>
                    </tr>
                    <tr>
                      <td id="open-8"><input type="radio" name="chr" id="chr-8" value="8">X</td>
                      <td id="open-9"><input type="radio" name="chr" id="chr-9" value="9">X</td>
                      <td id="open-10"><input type="radio" name="chr" id="chr-10" value="10">X</td>
                      <td id="open-11"><input type="radio" name="chr" id="chr-11" value="11">X</td>
                      <td id="open-12"><input type="radio" name="chr" id="chr-12" value="12">X</td>
                      <td id="open-13"><input type="radio" name="chr" id="chr-13" value="13">X</td>
                      <td id="open-14" class="use" data-toggle="tooltip" data-placement="top" title="Telah Dipesan"><input type="radio" name="chr" id="chr-14" value="14">X</td>
                      <td id="open-15"><input type="radio" name="chr" id="chr-15" value="15">X</td>

                    </tr>
                    <tr>
                      <td id="open-16"><input type="radio" name="chr" id="chr-16" value="16">X</td>
                      <td id="open-17"><input type="radio" name="chr" id="chr-17" value="17">X</td>
                      <td id="open-18"><input type="radio" name="chr" id="chr-18" value="18">X</td>
                      <td id="open-19"><input type="radio" name="chr" id="chr-19" value="19">X</td>
                      <td id="open-20"><input type="radio" name="chr" id="chr-20" value="20">X</td>
                      <td id="open-21"><input type="radio" name="chr" id="chr-21" value="21">X</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            -->
            <div class="theatre">
              <div class="cinema-seats left">
                <div class="cinema-row row-1">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-2">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-3">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-4">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-5">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-6">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-7">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>
              </div>


              <div class="cinema-seats right">
                <div class="cinema-row row-1">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-2">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-3">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-4">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-5">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-6">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>

                <div class="cinema-row row-7">
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                  <div class="seat"></div>
                </div>
              </div>
            </div>
          </div>
          <!--
          <div class="col-lg-4 event-info">
            <h4>Event Info</h4>
            <img src="img/wedding-couple.png" width="200px">
            <div class="row row-event">
              <div class="col-lg-2"><ion-icon name="pin"></ion-icon></div>
              <div class="col-lg-10">Keyongan RT 02/06 Nogosari Boyolali</div>
            </div>
            <div class="row">
              <div class="col-lg-2"><ion-icon name="time"></ion-icon></div>
              <div class="col-lg-10">Agt 20, 08:00 AM</div>
            </div>
          </div>
        -->
        <div class="row">
          <div class="col-lg-12">
            <input type="submit" name="submit" class="btnck">
            <input type="reset" name="reset" class="btnck">
          </div>
        </div>
      </div>
    </form>
    <div class="col-lg-12">
      <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
        <div class="col-lg-12" style="text-align:right;">
         <button class="btnext">Next</button>
       </div>
     </div>
   </div>
 </div>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>
