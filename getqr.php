<?php
include "lib/helper.php";
include "vendor/autoload.php";
use dbase\datafunction;

if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}

session_start();

if(!isset($_SESSION['client']['prebook'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['kode'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['nama'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['email'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['idusers'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['getqrcode'])){
  move("?page=index");
}
$o = new datafunction();

$data = $o->getDataAfterConfirm($_SESSION['client']['idusers'],$_SESSION['client']['kode']);
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="package/font-awesome/css/font-awesome.min.css">

  <script src="js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="package/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <title>Tamugo!</title>
</head>
<body>
  <div class="header">
    <nav class="navbar navbar-light bg-light shadow-sm">
      <a class="navbar-brand"><img src="img/icon.png" width="100px"></a>
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0 base" type="submit">Search</button>
      </form>
    </nav>
  </div>
  <div class="shadow-sm w-80 center">
    <div class="row contain">
      <div class="col-lg-12">
        <div class="row bs-wizard" style="border-bottom:0;">
          <div class="col-xs-3 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum">Langkah 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Konfirmasi Kehadiran</div>
          </div>

          <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 2</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Pilih Kursi / Tempat Duduk Anda</div>
          </div>

          <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 3</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cek Ulang Data</div>
          </div>

          <div class="col-xs-3 bs-wizard-step active"><!-- active -->
            <div class="text-center bs-wizard-stepnum"><b>Step 4</b></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center textactive">Cetak Kode QR</div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
          <div class="col-md-12">
            <h2>Selesai</h2>
            <div class="container text-center">
              <img id="qrcode" src="<?php echo $data['qrdata']; ?>" alt="qrcode">

              <br>
              <br>

              <button type="button" name="button" class="btnck" id="btnsimpan"><i class="fa fa-download"></i> Simpan</button>
            </div>

            <br>

            <blockquote class="blockquote text-center">
              <p class="mb-0">Terimakasih terlah melakukan konfirmasi, Simpan kode QR diatas sebagai bukti.</p>
              <footer class="blockquote-footer"><cite title="Source Title">Attract Ltd.</cite></footer>
            </blockquote>

          </div>
        </div>
        <div class="col-lg-12">
          <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
            <div class="col-lg-12" style="text-align:right;">
              <a href="?page=finish" id="nextverify"><button class="btnck">Finish</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

</body>
<script type="text/javascript">
$("document").ready(function(){
  $("#btnsimpan").click(function(){
    var src = $("#qrcode").attr("src");
    console.log(src);

    var a = $("<a>")
    .attr("href", src)
    .attr("download", "img.png")
    .appendTo("body");

    a[0].click();

    a.remove();
  });
});

</script>
</html>
