<?php
if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}
session_start();


if(!isset($_SESSION['client']['prebook'])){
  header("Location: ./");
  die();
}

if(!isset($_SESSION['client']['kode'])){
  header("Location: ./");
  die();
}

if(!isset($_SESSION['client']['nama'])){
  header("Location: ./");
  die();
}

if(!isset($_SESSION['client']['email'])){
  header("Location: ./");
  die();
}

if(!isset($_SESSION['client']['step'])){
  header("Location: ./");
  die();
}

if(!isset($_SESSION['client']['idusers'])){
  header("Location: ./");
  die();
}

require "vendor/autoload.php";
use dbase\datafunction;

$o = new datafunction();
if($o->confirmSeat($_SESSION['client'])){
  header("Location: ?page=step4");
  die();
} else{
  header("HTTP/1.1 500 Server Error");
  header("Content-type: text/json");

  die("Error Occured !, Please contact our Administrators");
}
?>
