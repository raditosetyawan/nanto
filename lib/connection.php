<?php
namespace dbase;
use mysqli;

class connection{
	private $host;
	private $user;
	private $pass;
	private $db;

	protected $conn;

	function __construct(){
		$this->host = "localhost";
		$this->user = "root";
		$this->pass = "";
		$this->db   = "event_db";

		try{
			$this->conn = new mysqli($this->host,$this->user,$this->pass,$this->db);
		}catch(Exception $z){
			throw $z;
		}
	}

}
?>
