<?php
namespace dbase;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

class connfunction extends connection{
	protected function allQuery($query){
		$socy = $this->conn->query($query);
		return $socy -> fetch_all(1);
	}
	protected function arrayQuery($query){
		$socy = $this->conn->query($query);
		return $socy -> fetch_array(1);
	}
	protected function countQuery($query){
		$socy = $this->conn->query($query);
		$num = $socy->num_rows;
		return $num;
	}
	protected function justQuery($query){
		$socy = $this->conn->query($query);
	}
	protected function justQueryes($query){
		$socy = $this->conn->query($query);
		if($socy){
			return true;
		} else{
			return false;
		}
	}
	protected function filter($s){
		return $this->conn->real_escape_string($s);
	}
	protected function seset($name,$val){
		$_SESSION[$name] = $val;
	}

	public function get($param){
		return $_GET[$param];
	}
	public function post($param){
		return $_POST[$param];
	}
	public function seslogin($usr,$name,$role){
		$sesid = sha1(rand().rand());

		$this->seset("sesid",$sesid);
		$this->seset("uname",$usr);
		$this->seset("name",$name);
		$this->seset("role",$role);
		$this->seset("loggedin","true");
	}
	public function isloggedin($true,$false){
		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
			//header("Location: ".$true);
		} else {
			header("Location: ".$false);
			exit(0);
		}
	}
	public function islogon(){
		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
			return true;
		} else {
			return false;
		}
	}
	public function isloggedinFront($true,$false){
		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
			header("Location: ".$true);
			exit(0);
		} else {
			//header("Location: ".$false);
		}
	}
	public function movePage($page){
		header("Location: ".$page);
		die();
	}
	public function redirect($page){
		header("Location: $page");
	}
	public function frmValid($arr){
		foreach($arr as $x){
			if(!isset($_POST[$x])){
				return false;
			}
		}
		return true;
	}
	public function lastId($col,$tbl){
		$query = "SELECT ($col+1) as last FROM $tbl ORDER BY $col DESC LIMIT 1";
		$tmp = $this->arrayQuery($query);
		if(empty($tmp["last"])){
			$i=1;
		} else{
			$i=$tmp["last"];
		}
		return $i;
	}
	public function generateQR($data){
		$options = new QROptions([
			'version'    => 5,
			'eccLevel'   => QRCode::ECC_L,
			'scale'			 => 7,
			'size'		   => 10
		]);
		$z = new QRCode($options);
		return $z->render($data);
	}
}
?>
