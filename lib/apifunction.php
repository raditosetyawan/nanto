<?php
namespace dbase;
class apifunction extends connfunction{
  public function getCanvasByID($id){
    $id = $this->filter($id);

    $q1 = $this->arrayQuery("SELECT version,active FROM event_canvas WHERE id = '$id'");
    $q2 = $this->allQuery("SELECT data FROM seat WHERE id_event = '$id'");

    $objects =[

    ];

    foreach($q2 as $q => $key){
      $objects[$q] = json_decode($key['data']);
    }


    $data = [
      "version" => $q1["version"],
      "active" => $q1["active"],
      "objects" => $objects
    ];

    return $data;

  }
  public function saveCanvas($data){
    $parse = (object) json_decode($data);

    $lastid  = $this->lastId("id","event_canvas");
    $lastsid = (int) $this->lastId("id","seat");

    $eid     = $this->filter($parse->event_id);
    $active  = $this->filter($parse->active);
    $ver     = $this->filter($parse->version);
    $obj     = !isset($parse->objects) ? [] : $parse->objects;

    $q = "INSERT INTO event_canvas SET id='$lastid',id_event='$eid',version='$ver',active='$active'";

    if(!$this->justQueryes($q)){
      return false;
    }

    foreach($obj as $o){
      $o=$this->filter(json_encode($o));

      $ax = "INSERT INTO seat SET id='$lastsid',id_event='$eid',data='$o'";

      if(!$this->justQueryes($ax)){
        return false;
      }
      $lastsid++;
    }

    return true;
  }
  public function getIdUser($data){
      $nama = $this->filter($data['nama']);
      $email = $this->filter($data['email']);
      $kode = $this->filter($data['kode']);

      $q = "SELECT event_users.id,event_users.nama,event_users.email FROM event_users INNER JOIN event ON event.id = event_users.id_event WHERE event.kode_event = '$kode' AND event_users.nama = '$nama' AND event_users.email = '$email' LIMIT 1";

      $x = $this->arrayQuery($q);

      return $x['id'];
    }
  public function chkUndangan($data){
    $kode = $this->filter($data['kode']);
    $nama = $this->filter($data['nama']);
    $email = $this->filter($data['email']);

    if(empty($kode)){
      return false;
    }
    if(empty($nama)){
      return false;
    }
    if(empty($email)){
      return false;
    }

    $iduser = $this->getIdUser($data);

    $q = "SELECT event_users.* FROM event_users INNER JOIN event ON event_users.id_event = event.id WHERE event.kode_event = '$kode' AND event_users.nama='$nama' AND event_users.email = '$email'";

    if($this->countQuery($q) > 0){
      $_SESSION['client']['prebook'] = true;
      $_SESSION['client']['kode'] = $kode;
      $_SESSION['client']['nama'] = $nama;
      $_SESSION['client']['email'] = $email;
      $_SESSION['client']['idusers'] = $iduser;

      $_SESSION['client']['step'] = 1;

      return true;
    }

    return false;
  }
  public function chkIfConfirmed($data){
    $kode = $this->filter($data['kode']);
    $nama = $this->filter($data['nama']);
    $email = $this->filter($data['email']);

    $q = "SELECT event_users.nama,event.kode_event FROM event INNER JOIN event_users ON event_users.id_event = event.id INNER JOIN event_confirm ON event_confirm.id_users = event_users.id WHERE event.kode_event = '$kode' AND event_users.nama = '$nama' AND event_users.email = '$email'";

    if($this->countQuery($q) > 0){
      unset($_SESSION['client']['step']);
      $_SESSION['client']['getqrcode'] = true;

      return true;
    }
    return false;

  }

  public function getDataFront($kode){
    $tmp = [];
    $kode = $this->filter($kode);

    $q = "SELECT *,event_canvas.version,event_canvas.active,seat.id as seatid FROM event RIGHT JOIN seat ON event.id = seat.id_event RIGHT JOIN event_canvas ON event_canvas.id_event = event.id WHERE event.kode_event = '$kode'";

    $datas = $this->allQuery($q);

    $i=0;
    $tmp["kode_event"] = $datas[0]["kode_event"];
    $tmp["nama_event"] = $datas[0]["nama_event"];
    $tmp["version"] = $datas[0]["version"];
    $tmp["active"] = $datas[0]["active"];

    foreach($datas as $data){
      $d = json_decode($data["data"],1);
      $d["id_seat"] = $data['seatid'];

      $tmp["objects"][$i] = $d;
      $i++;
    }

    array_walk_recursive($tmp, function(&$item, $key) {
      if ($item === '') $item = NULL;
      if ($item === "false") $item = false;
      if ($item === "true") $item = true;
    });

    return $tmp;
  }
  public function getIdEvent($kode){
    $kode = $this->filter($kode);

    $q = "SELECT id FROM event WHERE kode_event='$kode'";
    $q = $this->arrayQuery($q);

    return $q['id'];
  }
  public function deleteSeat($id){
    $id = $this->filter($id);

    $q = "DELETE FROM seat WHERE id = '$id'";

    return $this->justQueryes($q);
  }
  public function updateSeat($id,$json){
    $id = $this->filter($id);
    $json = $this->filter(json_encode($json));

    $q = "UPDATE seat SET data='$json' WHERE id='$id'";

    return $this->justQueryes($q);
  }
  public function addSeat(array $arr,$kode){
    $tmp = $arr;
    $id = $this->lastId("id","seat");
    $ide = $this->getIdEvent($kode);

    $tmp["id_seat"] = $id;

    $data = $this->filter(json_encode($tmp));

    $q = "INSERT INTO seat SET id='$id',id_event='$ide',data='$data',id_users=NULL,booked=0";

    return $this->justQueryes($q);
  }

  public function updateCanvas(array $data){
    $kode = $this->filter($data['kode']);
    $active = $this->filter($data['active']);
    $objects = $data['objects'];
    $del = $data['deleted'];

    $q = "UPDATE event_canvas SET active='$active' WHERE id_event = (SELECT id FROM event WHERE kode_event='$kode')";
    if(!$this->justQueryes($q))
      return false;

    foreach($del as $d){
      if(!$this->deleteSeat($d))
        return false;
    }
    foreach($objects as $o){
      if(!empty($o['id_seat'])){

        if(!$this->updateSeat($o['id_seat'],$o))
          return false;

      } else{
        if(!$this->addSeat($o,$kode))
          return false;
      }
    }

    return true;
  }
}
?>
