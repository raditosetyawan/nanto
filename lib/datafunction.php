<?php
namespace dbase;

class datafunction extends connfunction{
	public function loginadm($user,$pass){
		$user = $this->filter($user);
		$pass = $this->filter($pass);

		$q = "SELECT * FROM event_admin WHERE username='$user'";
		// echo $q;

		if($this->countQuery($q) > 0){
			$tmp = $this->arrayQuery($q);
			$pwd = $tmp['password'];

			if(password_verify($pass,$pwd)){
				$this->seslogin($user,$tmp['nama'],$tmp['role']);
				return true;
			} else{
				return false;
			}
		} else{
			return false;
		}
	}
	public function dataEvent($id=null){
		if($id == null){
			$q = "SELECT * FROM event";
		} else{
			$id = $this->filter($id);
			$q = "SELECT * FROM event WHERE id = '$id'";
		}

		$handle = $this->allQuery($q);
		return $handle;
	}
	public function dataEvents(){
		$q = "SELECT * FROM event LIMIT 10";

		$handle = $this->allQuery($q);
		return $handle;
	}
	public function eventProgress($id){
		$id = $this->filter($id);

		//$all  = $this->countQuery("SELECT event_users.*,seat.* FROM event_users INNER JOIN seat ON seat.id_users = event_users.id WHERE event_users.id_event = '$id'");
		//$book = $this->countQuery("SELECT event_users.*,seat.* FROM event_users INNER JOIN seat ON seat.id_users = event_users.id WHERE seat.booked = 1 AND event_users.id_event = '$id'");

		$q1 = "SELECT * FROM event_users WHERE id_event = '$id'";
		$q2 = "SELECT * FROM seat WHERE id_event = '$id' AND booked='1'";

		$total = $this->countQuery($q1);
		$confirm = $this->countQuery($q2);

		if($total == 0){
			$percent=0;
		} else{
			$percent = ($confirm/$total) * 100;
		}
		return $percent;
	}
	public function addEvent($data){
		$id = $this->lastId("id","event");
		$kode = $this->filter($data['kode']);
		$nama = $this->filter($data['nama']);
		$alamat = $this->filter($data['alamat']);
		$kontak = $this->filter($data['kontak']);
		$kapasitas = $this->filter($data['kapasitas']);

		// $dt = $data['date']." ".$data['time'];

		$date = $this->filter($data['date']);
		$time = $this->filter($data['time']);

		// $dt = $this->filter($dt);


		$query = "INSERT INTO event SET id='$id',kode_event='$kode',nama_event='$nama',alamat_event='$alamat',contact_event='$kontak',capacity_event='$kapasitas',date=concat('$date',' ',STR_TO_DATE('$time', '%l:%i %p'))";

		if($this->justQueryes($query)){
			return true;
		} else{
			return false;
		}
	}
	public function kodeEvent($id){
		$id = $this->filter($id);
		$q = "SELECT kode_event as kode FROM event WHERE id='$id' LIMIT 1";
		$tmp = $this->arrayQuery($q);

		return $tmp['kode'];
	}
	public function addTamu($data){
		$id = $this->lastId("id","event_users");
		$nama = $this->filter($data['nama']);
		$email = $this->filter($data['email']);
		$ide = $this->filter($data['idevent']);

		$q = "INSERT INTO event_users SET id='$id',id_event='$ide',nama='$nama',email='$email'";

		return $this->justQueryes($q);
	}
	public function isUserBook($id,$event){
		$id = $this->filter($id);
		$ide = $this->filter($event);
		$q = "SELECT * FROM seat WHERE id_event='$ide' AND id_users='$id'";
		if($this->countQuery($q) > 0){
			$tmp = $this->arrayQuery($q);

			if($tmp['booked'] !== 0){
				return true;
			}
		} else{
			return false;
		}
	}
	public function dataTamu($id){
		$id = $this->filter($id);
		$q = "SELECT event_users.*,seat.id as idseat,seat.booked FROM seat RIGHT JOIN event_users ON event_users.id_event = seat.id_event AND event_users.id = seat.id_users WHERE event_users.id_event='$id';";

		$tmp = $this->allQuery($q);

		return $tmp;
	}

	public function dashboardAnalytics(){
		$q1 = "SELECT * FROM event";
		$q2 = "SELECT * FROM event_users";

		$data = [
			"event" => $this->countQuery($q1),
			"event_users" => $this->countQuery($q2)
		];

		return $data;
	}

	public function canvasExist($eventid){
		$eventid = $this->filter($eventid);
		$q = "SELECT * FROM event_canvas WHERE id_event = '$eventid'";

		if($this->countQuery($q) > 0){
			return true;
		} else{
			return false;
		}
	}
	public function deleteCanvas($eventid){
		$id = $this->filter($eventid);

		$q1 = "DELETE FROM seat WHERE id_event='$id'";
		$q2 = "DELETE FROM event_canvas WHERE id_event='$id'";
		$q3 = "DELETE event_confirm FROM event_confirm INNER JOIN seat ON event_confirm.id_seat = seat.id WHERE seat.id_event='$id'";

		$this->justQuery($q3);
		$this->justQuery($q2);
		$this->justQuery($q1);
	}
	public function deleteEvent($eventid){
		$id = $this->filter($eventid);

		$q1 = "DELETE FROM seat WHERE id_event='$id'";
		$q2 = "DELETE FROM event_canvas WHERE id_event='$id'";
		$q3 = "DELETE event_confirm FROM event_confirm INNER JOIN seat ON event_confirm.id_seat = seat.id WHERE seat.id_event='$id'";
		$q4 = "DELETE FROM event WHERE id='$id'";
		$q5 = "DELETE FROM event_users WHERE id_event='$id'";

		$this->justQuery($q3);
		$this->justQuery($q2);
		$this->justQuery($q1);

		$this->justQuery($q5);
		$this->justQuery($q4);
	}
	public function getAdmin($id){
		$id = $this->filter($id);

		$q = "SELECT * FROM event_admin WHERE id='$id'";

		return $this->arrayQuery($q);
	}
	public function getAllAdmin(){
		$q = "SELECT * FROM event_admin";

		return $this->allQuery($q);
	}
	public function insertAdmin($data){
		$id = $this->lastId("id","event_admin");
		$nama = $this->filter($data['nama']);
		$uname = $this->filter($data['uname']);
		$passw = $this->filter(password_hash($data['passw'],PASSWORD_DEFAULT));
		$role = $this->filter($data['role']);

		$rolex = $role == "0" ? 'master' : 'admin';

		$q = "INSERT INTO event_admin SET id='$id',username='$uname',password='$passw',nama='$nama',role='$rolex'";

		if($this->justQueryes($q)){
			return true;
		} else{
			return false;
		}
	}
	public function delAdmin($id){
		$id = $this->filter($id);

		$q = "DELETE FROM event_admin WHERE id='$id'";

		if($this->justQueryes($q)){
			return true;
		} else{
			return false;
		}
	}
	public function updAdmin($data){
		$role = $data['role'] == "0" ? 'master' : 'admin';
		$nama = $this->filter($data['nama']);
		$uname = $this->filter($data['uname']);
		$passw = $this->filter(password_hash($data['passw'],PASSWORD_DEFAULT));
		$id = $this->filter($data['foo']);
		// $role = $this->filter($data['role']);

		if(empty($data['passw'])){
			$q = "UPDATE event_admin SET nama='$nama',username='$uname',role='$role' WHERE id='$id'";
		} else{
			$q = "UPDATE event_admin SET nama='$nama',username='$uname',password='$passw',role='$role' WHERE id='$id'";
		}

		if($this->justQueryes($q)){
			return true;
		}

		return false;
	}
	public function getEventByKode($kode){
		$kode = $this->filter($kode);
		$q = "SELECT * FROM event WHERE kode_event = '$kode' LIMIT 1";

		$data = $this->arrayQuery($q);

		return $data;
	}
	public function confirmSeatJson($nama,$id,$active){
		$id = $this->filter($id);
		$nama = $this->filter($nama);

		$q = "SELECT * FROM seat WHERE id = '$id'";
		$q = $this->arrayQuery($q);

		$data = json_decode($q['data'],1);
		$data['used'] = "true";
		$data['booked'] = "true";
		$data['bookedby'] = $nama;
		$data['fill'] = $active;

		return json_encode($data);
	}
	public function afterConfirm($data){
		date_default_timezone_set("Asia/Jakarta");

		$id = $this->lastId("id","event_confirm");
		$uid = $this->filter($data['idusers']);
		$sid = $this->filter($data['seat']);
		$date = date('Y-m-d H:i:s');

		$seed = bin2hex(random_bytes(16));

		$data = 'AT'.$id.'-'.$seed;

		$qr = $this->filter($this->generateQR($data));

		$q = "INSERT INTO event_confirm SET id='$id',id_users='$uid',id_seat='$sid',date='$date',seed='$seed',qrdata='$qr'";

		if($this->justQueryes($q)){
			$_SESSION['client']['step'] = 3;
			$_SESSION['client']['idconfirm'] = $id;
			return true;
		}
		return false;

	}
	public function confirmSeat($data){
		$kode = $this->filter($data['kode']);
		$seatid = $this->filter($data['seat']);
		$uid = $this->filter($data['idusers']);

		$chk = $this->countQuery("SELECT * FROM seat WHERE id = '$seatid' AND booked = 0");

		if($chk <= 0){
			return false;
		}

		$fill = $this->arrayQuery("SELECT event_canvas.*,event.nama_event FROM event INNER JOIN event_canvas ON event_canvas.id_event = event.id WHERE event.kode_event = '$kode'");
		$fill = $fill['active'];

		$seat = $this->filter($this->confirmSeatJson($data['nama'],$data['seat'],$fill));

		$update = "UPDATE seat SET data = '$seat',id_users='$uid',booked=1 WHERE id='$seatid'";

		if($this->justQueryes($update)){
			if(!$this->afterConfirm($data)){
				return false;
			}
			return true;
		} else{
			return false;
		}

	}
	public function dataConfirm($id){
		$id = $this->filter($id);

		$q = "SELECT * FROM event_confirm WHERE id='$id'";
		$q = $this->arrayQuery($q);
		return $q;
	}
	public function getDataAfterConfirm($idu,$kode){
		$idu = $this->filter($idu);
		$kode = $this->filter($kode);

		$q = "SELECT event_confirm.* FROM event_confirm INNER JOIN seat ON event_confirm.id_seat = seat.id INNER JOIN event ON event.id = seat.id_event WHERE event_confirm.id_users = '$idu' AND event.kode_event = '$kode'";

		return $this->arrayQuery($q);
	}
	public function dataConfirmAdmin($uid,$sid){
		$uid = $this->filter($uid);
		$sid = $this->filter($sid);

		$q= "SELECT event_confirm.*,event_users.nama,event_users.email FROM event_confirm INNER JOIN event_users ON event_users.id = event_confirm.id_users WHERE event_confirm.id_users='$uid' AND event_confirm.id_seat = '$sid'";

		return $this->arrayQuery($q);
	}
	public function getEventIDBySeatCode($uid){
		$uid = $this->filter($uid);

		$q = "SELECT id_event FROM seat WHERE id_users='$uid' AND booked=1";
		$q = $this->arrayQuery($q);

		return $q['id_event'];
	}
	public function chkIfConfirmed($idu,$ids){
		$idu = $this->filter($idu);
		$ids = $this->filter($ids);

		$q = "SELECT * FROM event_confirm WHERE id_users='$idu' AND id_seat='$ids'";

		if($this->countQuery($q) > 0){
			return true;
		}

		return false;
	}
	public function eventEdit($data){
		$kode = $this->filter($data['evkode']);
		$nama = $this->filter($data['evnama']);
		$alamat = $this->filter($data['evalamat']);
		$kontak = $this->filter($data['evkontak']);
		$kapasitas = $this->filter($data['evkapasitas']);

		$tgl = $this->filter($data['evtgl']);
		$jam = $this->filter($data['evjam']);

		$date = $this->filter($tgl." ".$jam);

		$q = "UPDATE event SET nama_event='$nama',alamat_event='$alamat',contact_event='$kontak',capacity_event='$kapasitas',date=concat('$tgl',' ',STR_TO_DATE('$jam', '%l:%i %p')) WHERE kode_event = '$kode'";

		return $this->justQueryes($q);
	}
	public function dataUser($id = null){
		if($id !== null){
			$id = $this->filter($id);

			$q = "SELECT * FROM event_users WHERE id='$id'";
		} else{

			$q = "SELECT * FROM event_users";

		}

		return $this->allQuery($q);
	}
	public function userEdit($data){
		$id = $this->filter($data['_iduser']);
		$nama = $this->filter($data['evnama']);
		$email = $this->filter($data['evemail']);

		$q = "UPDATE event_users SET nama='$nama',email='$email' WHERE id='$id'";

		return $this->justQueryes($q);
	}
	public function userExist($id,$ide){
		$id = $this->filter($id);
		$ide = $this->filter($ide);

		$q = "SELECT * FROM event_users WHERE id='$id' AND id_event='$ide'";

		if($this->countQuery($q) > 0){
			return true;
		}
		return false;
	}
	public function deleteUser($id){
		$id = $this->filter($id);

		$qa = "SELECT data FROM seat WHERE id_users='$id' AND booked=1";
		$qa = $this->arrayQuery($qa);

		$qa = (array) json_decode($qa['data']);

		$qa['fill'] = $qa['basefill'];
		$qa['booked'] = "false";
		$qa['used'] = "false";
		$qa['bookedby'] = "";

		$qa = $this->filter(json_encode($qa));

		$q1 = "UPDATE seat SET data='$qa',booked=0,id_users = NULL WHERE id_users='$id'";
		$q2 = "DELETE FROM event_confirm WHERE id_users='$id'";
		$q3 = "DELETE FROM event_users WHERE id='$id'";

		$this->justQuery($q1);
		$this->justQuery($q2);
		$this->justQuery($q3);
	}
}
?>
