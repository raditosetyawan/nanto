<?php
if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}

session_start();

$ses = [
  "prebook",
  "kode",
  "nama",
  "email",
  "step",
  "seat",
  "idusers",
  "idconfirm",
  "getqrcode"
];
foreach($ses as $se){

  if(isset($_SESSION['client'][$se])){
    unset($_SESSION['client'][$se]);
  }
}
header("Location: ./");
?>
