$("document").ready(function(){
  var canvas = new fabric.Canvas('c');

  var cwidth = canvas.getWidth();
  var cheight = canvas.getHeight();

  var grid = 40;
  var grdx;

  setGrid(cwidth,grid);

  $("input[name=width]").bind('input',function(){
    grid = $(this).val();
    clearGrid();
    setGrid(cwidth,grid);
  });

  function clearGrid(){
    var tmp = canvas.getObjects('line');
    for(let z in tmp){
      canvas.remove(tmp[z]);
    }
  }

  function setGrid(cwidth,grid){
    $("input[name=width]").val(grid);
    grdx = parseInt(grid);
    for (var i = 0; i < (cwidth / grid); i++) {
      canvas.add(new fabric.Line([i * grid, 0, i * grid, cwidth], {
        stroke: '#f0f0f0',
        grid : 'true',
        selectable: false
      }));
      canvas.add(new fabric.Line([0, i * grid, cwidth, i * grid], {
        stroke: '#f0f0f0',
        grid : 'true',
        selectable: false
      }))
    }
  }

  canvas.on('mouse:up', function(opt){
    //console.log(JSON.stringify(opt));
  });
  canvas.on('mouse:down',function(e){
    /*
    var tmp = canvas.getActiveObject();
    tmp.fill = "blue";

    canvas.remove(tmp);
    console.log(tmp);
    */
    //canvas.add(tmp);
    //console.log("click");
    //console.log(e);
  });
  canvas.on('mouse:dblclick', function(e){
    //console.clear();
    //console.log(e.target);
  });

  // Change grid on scale
  canvas.on('object:scaling', function(e){
    //console.log(e);
  });


  $("#rect").click(function(){
    var x = (parseInt(cwidth)/grdx)+10;
    var y = (parseInt(cheight)/grdx)-13;

    console.log(grdx);
    var rect = new fabric.Rect({
      left: x,
      top: y,
      fill: 'red',
      width: grdx,
      height: grdx
    });

    canvas.add(rect);
    //console.log(canvas.getObjects());

  });
  $("#circ").click(function(){
    var rect = new fabric.Circle({
      left: 100,
      top: 50,
      fill: '#D81B60',
      width: grid,
      height: grid,
      radius: 20
    });

    canvas.add(rect);

  });
  $("#clear").click(function(){
    canvas.clear();
    setGrid(cwidth,grid);
  });

  canvas.on('object:moving', function(options) {
    if (Math.round(options.target.left / grid * 4) % 4 == 0 &&
    Math.round(options.target.top / grid * 4) % 4 == 0) {
      options.target.set({
        left: Math.round(options.target.left / grid) * grid,
        top: Math.round(options.target.top / grid) * grid
      }).setCoords();
    }
  });

  $(document).keydown(function(e) {
    var temp;
    if (e.keyCode == 46) {
      e.preventDefault();
      // Fungsi Hapus Objek

      if(canvas.getActiveObject()._objects == undefined){
        // Jika objek hanya 1

        canvas.remove(canvas.getActiveObject());
      } else {
        // Jika Objek lebih dari 1
        console.log(canvas.getActiveObject());
        var z = canvas.getActiveObject()._objects;

        for(x in z){
          canvas.remove(z[x]);
        }
      }

    }
    if (e.keyCode == 67 && e.ctrlKey) {
      e.preventDefault();
      console.log("ctrl + c");
      canvas.getActiveObject().clone(function(cloned) {
        _clipboard = cloned;
      });
    }
    if (e.keyCode == 86 && e.ctrlKey) {
      e.preventDefault();
      console.log("ctrl + v");

      _clipboard.clone(function(clonedObj) {
        canvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + grid*2,
          top: clonedObj.top,
          evented: true,
        });
        if (clonedObj.type === 'activeSelection') {
          // active selection needs a reference to the canvas.
          clonedObj.canvas = canvas;
          clonedObj.forEachObject(function(obj) {
            canvas.add(obj);
          });
          // this should solve the unselectability
          clonedObj.setCoords();
        } else {
          canvas.add(clonedObj);
        }
        _clipboard.top;
        _clipboard.left += grid*2;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
      });
    }

  });
  console.log(canvas.getObjects());
  $(window).resize(function(e) {
    $("#c").attr("width",$(document).innerWidth()+"px");
    console.log($("#c").attr("width"));
  });
  var type;

  function makeCircle(data){
    var z = new fabric.Circle(data);
    return z;
  }
  function makeRect(data){
    var z = new fabric.Rect(data);
    return z;
  }

  var currentlyDragging;

  function handleDragStart(e) {
    //console.log("start",e.srcElement.id);
    type = e.srcElement.id;
    [].forEach.call(images, function (img) {
      img.classList.remove('img_dragging');
    });
    this.classList.add('img_dragging');
    currentlyDragging = e.target;
  }

  function handleDragOver(e) {
    //console.log(e);
    if (e.preventDefault) {
      e.preventDefault(); // Necessary. Allows us to drop.
    }

    e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
    // NOTE: comment above refers to the article (see top) -natchiketa

    return false;
  }

  function handleDragEnter(e) {
    // this / e.target is the current hover target.
    this.classList.add('over');
    console.log(e);
  }

  function handleDragLeave(e) {
    this.classList.remove('over'); // this / e.target is previous target element.
  }

  function handleDrop(e) {

    if (e.preventDefault) {
      e.preventDefault();
    }

    if (e.stopPropagation) {
      e.stopPropagation(); // stops the browser from redirecting.
    }

    var ext = currentlyDragging.src.substr(-3);

    console.log(e.layerX + ", " + e.layerY);

    switch(type){
      case "circle":
      sx = makeCircle({
        left: e.layerX,
        top: e.layerY,
        fill: '#D81B60',
        width: grdx,
        height: grdx,
        radius: grdx/2
      });
      break;

      case "rectx":
      sx = makeRect({
        left: e.layerX,
        top: e.layerY,
        fill: 'red',
        width: grdx,
        height: grdx
      });
      break;
    }

    canvas.add(sx);
    return true;
  }

  function handleDragEnd(e) {
    // this/e.target is the source node.
    [].forEach.call(images, function (img) {
      img.classList.remove('img_dragging');
    });
  }

  if (Modernizr.draganddrop) {
    // Browser supports HTML5 DnD.
    console.log("drag");
    // Bind the event listeners for the image elements
    var images = $("img");

    var objects = $('#images object');
    [].forEach.call(images, function (img) {
      img.addEventListener('dragstart', handleDragStart, false);
      img.addEventListener('dragend', handleDragEnd, false);
    });
    [].forEach.call(objects, function (obj) {
      obj.addEventListener('dragstart', handleDragStart, false);
      obj.addEventListener('dragend', handleDragEnd, false);
    });
    // Bind the event listeners for the canvas
    var canvasContainer = document.getElementById('canvas-container');
    canvasContainer.addEventListener('dragenter', handleDragEnter, false);
    canvasContainer.addEventListener('dragover', handleDragOver, false);
    canvasContainer.addEventListener('dragleave', handleDragLeave, false);
    canvasContainer.addEventListener('drop', handleDrop, false);
  } else {
    // Replace with a fallback to a library solution.
    alert("This browser doesn't support the HTML5 Drag and Drop API.");
  }
});
