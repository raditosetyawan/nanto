$("document").ready(function(){

  var canvas = new fabric.Canvas('c');
  var cs = $("#c");
  var canvaz = document.querySelector("canvas");
  var cwidth = canvas.getWidth();
  var cheight = canvas.getHeight();
  var grid = 40;
  var tmpc = '#ff0000';
  var grdx;

  function exportJSON(json){
    if(json.objects.length > 0){
      var tmp = [];

      for(object in json.objects){
        if(json.objects[object].type != "line"){
          json.objects[object].used          = false;
          json.objects[object].booked        = false;
          json.objects[object].bookedby      = "";

          json.objects[object].lockRotation  = true;

          json.objects[object].lockScalingY  = true;
          json.objects[object].lockScalingX  = true;

          json.objects[object].lockMovementX = true;
          json.objects[object].lockMovementY = true;

          json.objects[object].hasControls   = false;
          json.objects[object].hasBorders    = false;

          json.objects[object].hoverCursor   = "pointer";

          json.objects[object].basefill      = json.objects[object].fill;

          tmp.push(json.objects[object]);
        }
      }
    }
    var data = {};
    data["version"] = "2.3.4";
    data["active"] = $("#obj-active").val();
    data["event_id"] = $("#_id").val();
    data["objects"] = tmp;
    console.log(tmp);

    return data;
  }
  $("#save").click(function(){
    var final = exportJSON(canvas.toJSON());
    // console.log(final);

    $.ajax({
      url: '../../API/back/canvas',
      type: 'POST',
      data: final,
      success:function(res){
        console.log(res);
        if(res.status == "success"){
          swal({
            type: 'success',
            title: 'Sukses',
            text: 'Data berhasil disimpan',
          }).then((result) => {
            window.location.href = "event_view.php?id="+$("#_id").val();
          })
        } else{
          swal({
            type: 'error',
            title: 'Gagal',
            text: 'Data gagal disimpan',
          })
        }
      },
      error:function(x,y,z){
        console.log(x,y,z);
      }
    });

    //console.log(canvas.toJSON());
  });
  $("#obj-color").ColorPicker({
    color: tmpc,
    onBeforeShow: function(el){
      $(this).ColorPickerSetColor(tmpc);
    },
    onChange: function (hsb, hex, rgb) {
      $(this).val('#' + hex);
      $("#obj-color").val('#'+hex);
      tmpc = '#'+hex;
      $(this).ColorPickerSetColor(tmpc);
      itmcolor('#'+hex);
    },
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      itmcolor("#"+hex);
      $(el).ColorPickerHide();
    }
  });
  $("#obj-active").ColorPicker({
    color: $(this).val(),
    onBeforeShow: function(el){
      $(this).ColorPickerSetColor($(this).val());
    },
    onChange: function (hsb, hex, rgb) {
      $(this).val('#' + hex);
      $("#obj-active").val('#'+hex);
      tmpc = '#'+hex;
      $(this).ColorPickerSetColor(tmpc);
    },
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      $(el).ColorPickerHide();
    }
  });

  window.addEventListener('resize', resizeCanvas, false);
  resizeCanvas();

  setGrid(cwidth,grid);

  function itmcolor(clr){
    var active = canvas.getActiveObject();
    if(active._objects == undefined){
      canvas.getActiveObject().set("fill",clr);
    } else{
      for(tmp in active._objects){
        active._objects[tmp].set("fill",clr);
      }
    }
    canvas.renderAll();
  }

  function resizeCanvas() {
    var bwidth = cs.parent().parent().css("width");
    canvas.setHeight(window.innerHeight);
    canvas.setWidth(parseInt(bwidth)-20);
    canvas.renderAll();

    clearGrid();
    setGrid(canvas.getWidth(),grid);
  }

  $(".sidebar-toggle").click(function(){
    //console.log("toggle");
    setTimeout(function(){
      resizeCanvas();
    },500)

  });
  $("input[name=width]").bind('input',function(){
    grid = $(this).val();
    clearGrid();
    setGrid(cwidth,grid);
  });
  function fitToContainer(canvas){
    // Make it visually fill the positioned parent
    canvas.style.width ='100%';
    canvas.style.height='100%';
    // ...then set the internal size to match
    canvas.width  = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
  }
  function clearGrid(){
    var tmp = canvas.getObjects('line');
    for(let z in tmp){
      canvas.remove(tmp[z]);
    }
  }

  function setGrid(cwidth,grid){
    $("input[name=width]").val(grid);
    grdx = parseInt(grid);
    for (var i = 0; i < (cwidth / grid); i++) {
      canvas.add(new fabric.Line([i * grid, 0, i * grid, cwidth], {
        stroke: '#f0f0f0',
        grid : 'true',
        selectable: false
      }));
      canvas.add(new fabric.Line([0, i * grid, cwidth, i * grid], {
        stroke: '#f0f0f0',
        grid : 'true',
        selectable: false
      }))
    }
  }
  function optionFill(){
    var tmp = canvas.getActiveObject();
    if(tmp != null){
      $("#obj-color").val(tmp.fill);
      $("#obj-wh").val(Math.floor(tmp.width*tmp.scaleX)+" x "+Math.floor(tmp.height*tmp.scaleY));
      $("#obj-coord").val(tmp.left+" , "+tmp.top);
      tmpc = tmp.fill;
      //console.log(tmpc);
    } else{
      $("#obj-color").val("");
      $("#obj-wh").val("");
      $("#obj-coord").val("");
    }
  }

  canvas.on('mouse:up', function(opt){
    ////console.log(JSON.stringify(opt));
  });
  canvas.on('mouse:down',function(e){
    optionFill();
    /*
    var tmp = canvas.getActiveObject();
    tmp.fill = "blue";

    canvas.remove(tmp);
    //console.log(tmp);
    */
    //canvas.add(tmp);
    ////console.log("click");
    ////console.log(e);
  });
  canvas.on('mouse:dblclick', function(e){
    //console.clear();
    ////console.log(e.target);
  });

  // Change grid on scale
  canvas.on('object:scaling', function(e){
    optionFill();
  });


  $("#rect").click(function(){
    var x = (parseInt(cwidth)/grdx)+10;
    var y = (parseInt(cheight)/grdx)-13;

    //console.log(grdx);
    var rect = new fabric.Rect({
      left: x,
      top: y,
      fill: '#ff0000',
      width: grdx,
      height: grdx
    });

    canvas.add(rect);
    ////console.log(canvas.getObjects());

  });
  $("#circ").click(function(){
    var rect = new fabric.Circle({
      left: 100,
      top: 50,
      fill: '#0000ff',
      width: grid,
      height: grid,
      radius: 20
    });

    canvas.add(rect);

  });
  $("#clear").click(function(){
    canvas.clear();
    //setGrid(cwidth,grid);
    resizeCanvas();
  });

  canvas.on('object:moving', function(options) {
    if (Math.round(options.target.left / grid * 4) % 4 == 0 &&
    Math.round(options.target.top / grid * 4) % 4 == 0) {
      options.target.set({
        left: Math.round(options.target.left / grid) * grid,
        top: Math.round(options.target.top / grid) * grid
      }).setCoords();
    }
  });

  $(document).keydown(function(e) {
    var temp;
    // Fungsi Hapus Objek

    if (e.keyCode == 46) {
      e.preventDefault();
      if(canvas.getActiveObject()._objects == undefined){
        // Jika objek hanya 1

        canvas.remove(canvas.getActiveObject());
      } else {
        // Jika Objek lebih dari 1
        //console.log(canvas.getActiveObject());
        var z = canvas.getActiveObject()._objects;

        for(x in z){
          canvas.remove(z[x]);
        }
      }
      optionFill();
    }
    // CTRL + C KEY

    if (e.keyCode == 67 && e.ctrlKey) {
      e.preventDefault();
      canvas.getActiveObject().clone(function(cloned) {
        _clipboard = cloned;
      });
    }

    // CTRL + V KEY
    if (e.keyCode == 86 && e.ctrlKey) {
      e.preventDefault();
      _clipboard.clone(function(clonedObj) {
        canvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + grid*2,
          top: clonedObj.top,
          evented: true,
        });
        if (clonedObj.type === 'activeSelection') {

          clonedObj.canvas = canvas;
          clonedObj.forEachObject(function(obj) {
            canvas.add(obj);
          });

          clonedObj.setCoords();
        } else {
          canvas.add(clonedObj);
        }
        _clipboard.top;
        _clipboard.left += grid*2;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
      });
      optionFill();
    }

  });
  //console.log(canvas.getObjects());
  $(window).resize(function(e) {
    //$("#c").attr("width",$(document).innerWidth()+"px");
    ////console.log($("#c").attr("width"));
  });
  var type;

  function makeCircle(data){
    var z = new fabric.Circle(data);
    return z;
  }
  function makeRect(data){
    var z = new fabric.Rect(data);
    return z;
  }

  var currentlyDragging;

  function handleDragStart(e) {
    ////console.log("start",e.srcElement.id);
    type = e.srcElement.id;
    [].forEach.call(images, function (img) {
      img.classList.remove('img_dragging');
    });
    this.classList.add('img_dragging');
    //currentlyDragging = e.target;
  }

  function handleDragOver(e) {
    ////console.log(e);
    if (e.preventDefault) {
      e.preventDefault(); // Necessary. Allows us to drop.
    }

    e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
    // NOTE: comment above refers to the article (see top) -natchiketa

    return false;
  }

  function handleDragEnter(e) {
    // this / e.target is the current hover target.
    this.classList.add('over');
    //console.log(e);
  }

  function handleDragLeave(e) {
    this.classList.remove('over'); // this / e.target is previous target element.
  }

  function handleDrop(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }

    if (e.stopPropagation) {
      e.stopPropagation(); // stops the browser from redirecting.
    }

    //var ext = currentlyDragging.src.substr(-3);
    var sx;

    var xv;
    var yv;


    switch(type){
      case "circle":
      sx = makeCircle({
        left: e.layerX,
        top: e.layerY,
        fill: '#0000ff',
        width: grdx,
        height: grdx,
        radius: grdx/2,
        obj : true
      });
      break;

      case "rectx":
      sx = makeRect({
        left: e.layerX,
        top: e.layerY,
        fill: '#ff0000',
        width: grdx,
        height: grdx,
        obj : true
      });
      break;
    }

    var jml = parseInt(prompt("Masukkan jumlah :"));

    if(!isNaN(jml)){
      xv = e.layerX;
      yv = e.layerY;

      for(let z=0;z<jml;z++){
        switch(type){
          case "circle":
          sx = makeCircle({
            left: xv,
            top: yv,
            fill: '#0000ff',
            width: grdx,
            height: grdx,
            radius: grdx/2,
            obj : true
          });
          break;

          case "rectx":
          sx = makeRect({
            left: xv,
            top: yv,
            fill: '#ff0000',
            width: grdx,
            height: grdx,
            obj : true
          });
          break;
        }

        canvas.add(sx);

        xv+=grid*2;
      }
    } else{
      canvas.add(sx);
    }

    canvas.getObjects('line');
    return true;
  }

  function handleDragEnd(e) {
    // this/e.target is the source node.
    [].forEach.call(images, function (img) {
      img.classList.remove('img_dragging');
    });
  }

  if (Modernizr.draganddrop) {
    // Browser supports HTML5 DnD.
    //console.log("drag");
    // Bind the event listeners for the image elements
    var images = $("img");

    var objects = $('#images object');
    [].forEach.call(images, function (img) {
      img.addEventListener('dragstart', handleDragStart, false);
      img.addEventListener('dragend', handleDragEnd, false);
    });
    [].forEach.call(objects, function (obj) {
      obj.addEventListener('dragstart', handleDragStart, false);
      obj.addEventListener('dragend', handleDragEnd, false);
    });
    // Bind the event listeners for the canvas
    var canvasContainer = document.getElementById('box-body');
    canvasContainer.addEventListener('dragenter', handleDragEnter, false);
    canvasContainer.addEventListener('dragover', handleDragOver, false);
    canvasContainer.addEventListener('dragleave', handleDragLeave, false);
    canvasContainer.addEventListener('drop', handleDrop, false);
  } else {
    // Replace with a fallback to a library solution.
    alert("This browser doesn't support the HTML5 Drag and Drop API.");
  }

});
