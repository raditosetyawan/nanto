$("document").ready(function(){
  var canvasid = $("#_idevent").val();
  var canvas = new fabric.Canvas('c');
  var cs = $("#c");
  var cwidth = canvas.getWidth();
  var cheight = canvas.getHeight();
  var json = '';
  var grid = 40;
  var tmpc = '#ff0000';
  var deleted = [];
  var ver;
  var kodeevent;
  var dataglobal;


  function exportJSON(json){
    if(json.objects.length > 0){
      var tmp = [];

      for(object in json.objects){
        if(json.objects[object].type != "line"){
          json.objects[object].used          = false;
          json.objects[object].booked        = false;
          json.objects[object].bookedby      = "";

          json.objects[object].lockRotation  = true;

          json.objects[object].lockScalingY  = true;
          json.objects[object].lockScalingX  = true;

          json.objects[object].lockMovementX = true;
          json.objects[object].lockMovementY = true;

          json.objects[object].hasControls   = false;
          json.objects[object].hasBorders    = false;

          json.objects[object].hoverCursor   = "pointer";

          json.objects[object].basefill      = json.objects[object].fill;

          tmp.push(json.objects[object]);
        }
      }
    }
    var data = {};
    data["version"] = "2.3.4";
    data["active"] = $("#obj-active").val();
    data["event_id"] = $("#_id").val();
    data["objects"] = tmp;

    return data;
  }

  function resizeCanvas() {
    var bwidth = cs.parent().parent().css("width");
    canvas.setHeight(window.innerHeight);
    canvas.setWidth(parseInt(bwidth)-20);
    canvas.renderAll();

    clearGrid();
    setGrid(canvas.getWidth(),grid);
  }
  function setGrid(cwidth,grid){
    $("input[name=width]").val(grid);
    grdx = parseInt(grid);
    for (var i = 0; i < (cwidth / grid); i++) {
      canvas.add(new fabric.Line([i * grid, 0, i * grid, cwidth], {
        stroke: '#f0f0f0',
        grid : 'true',
        selectable: false
      }));
      canvas.add(new fabric.Line([0, i * grid, cwidth, i * grid], {
        stroke: '#f0f0f0',
        grid : 'true',
        selectable: false
      }))
    }
  }
  function clearGrid(){
    var tmp = canvas.getObjects('line');
    for(let z in tmp){
      canvas.remove(tmp[z]);
    }
  }
  function parseJSON(json){
    var jsons = json
    for(x in jsons.objects){
      jsons.objects[x].lockRotation  = false;

      jsons.objects[x].lockScalingY  = true;
      jsons.objects[x].lockScalingX  = false;

      jsons.objects[x].lockMovementX = false;
      jsons.objects[x].lockMovementY = false;

      jsons.objects[x].hasControls   = true;
      jsons.objects[x].hasBorders    = true;

      jsons.objects[x].hoverCursor   = false;
    }

    return jsons;
  }
  function optionFill(){
    var tmp = canvas.getActiveObject();
    if(tmp != null){
      $("#obj-color").val(tmp.fill);
      $("#obj-wh").val(Math.floor(tmp.width*tmp.scaleX)+" x "+Math.floor(tmp.height*tmp.scaleY));
      $("#obj-coord").val(tmp.left+" , "+tmp.top);
      $("#obj-book").val(tmp.bookedby);
      tmpc = tmp.fill;
      //console.log(tmpc);
    } else{
      $("#obj-color").val("");
      $("#obj-wh").val("");
      $("#obj-coord").val("");
      $("#obj-book").val("");
    }
  }
  function itmcolor(clr){
    var active = canvas.getActiveObject();

    if(!active._objects){
      if(active.booked !== true){
        canvas.getActiveObject().set("fill",clr);
        canvas.getActiveObject().set("basefill",clr);
      }
      canvas.getActiveObject().set("basefill",clr);
    } else{
      for(tmp in active._objects){
        if(active._objects[tmp].booked !== true){
          active._objects[tmp].set("fill",clr);
          active._objects[tmp].set("basefill",clr);
        }

        active._objects[tmp].set("basefill",clr);
      }
    }
    canvas.renderAll();
  }
  function makeCircle(data){
    var z = new fabric.Circle(data);
    return z;
  }
  function makeRect(data){
    var z = new fabric.Rect(data);
    return z;
  }
  function customsave(callback){
    canvas.discardActiveObject();

    var tmp = canvas.getObjects();
    var tmps = [];
    var data = {};
    var tmpn = {};

    for(z in tmp){
      if(tmp[z].type != "line"){
        //console.log(tmp[z]);
        tmpn["type"] = tmp[z].type;
        tmpn["version"] = tmp[z].version == undefined ? "2.3.4" : tmp[z].version;
        tmpn["originX"] = tmp[z].originX;
        tmpn["originY"] = tmp[z].originY;
        tmpn["left"] = tmp[z].left;
        tmpn["top"] = tmp[z].top;
        tmpn["width"] = tmp[z].width;
        tmpn["height"] = tmp[z].height;
        tmpn["fill"] = tmp[z].fill;
        tmpn["stroke"] = tmp[z].stroke;
        tmpn["strokeWidth"] = tmp[z].strokeWidth;
        tmpn["strokeDashArray"] = tmp[z].strokeDashArray;
        tmpn["strokeLineCap"] = tmp[z].strokeLineCap;
        tmpn["strokeLineJoin"] = tmp[z].strokeLineJoin;
        tmpn["strokeMiterLimit"] = tmp[z].strokeMiterLimit;
        tmpn["scaleX"] = tmp[z].scaleX;
        tmpn["scaleY"] = tmp[z].scaleY;
        tmpn["angle"] = tmp[z].angle;
        tmpn["flipX"] = tmp[z].flipX;
        tmpn["flipY"] = tmp[z].flipY;
        tmpn["opacity"] = tmp[z].opacity;
        tmpn["shadow"] = tmp[z].shadow;
        tmpn["visible"] = tmp[z].visible;
        tmpn["clipTo"] = tmp[z].clipTo;
        tmpn["backgroundColor"] = tmp[z].backgroundColor;
        tmpn["fillRule"] = tmp[z].fillRule;
        tmpn["paintFirst"] = tmp[z].paintFirst;
        tmpn["globalCompositeOperation"] = tmp[z].globalCompositeOperation;
        tmpn["transformMatrix"] = tmp[z].transformMatrix;
        tmpn["skewX"] = tmp[z].skewX;
        tmpn["skewY"] = tmp[z].skewY;
        tmpn["radius"] = tmp[z].radius == undefined ? null : tmp[z].radius;
        tmpn["startAngle"] = tmp[z].startAngle == undefined ? null : tmp[z].startAngle;
        tmpn["endAngle"] = tmp[z].endAngle == undefined ? null : tmp[z].endAngle;
        tmpn["used"] = tmp[z].used == undefined ? false : tmp[z].used;
        tmpn["booked"] = tmp[z].booked == undefined ? false : tmp[z].booked;
        tmpn["bookedby"] = tmp[z].bookedby == undefined ? "" : tmp[z].bookedby;
        tmpn["lockRotation"] = true;
        tmpn["lockScalingY"] = true;
        tmpn["lockScalingX"] = true;
        tmpn["lockMovementX"] = true;
        tmpn["lockMovementY"] = true;
        tmpn["hasControls"] = false;
        tmpn["hasBorders"] = false;
        tmpn["hoverCursor"] = "pointer";
        tmpn["id_seat"] =  tmp[z].id_seat == undefined ? "" : tmp[z].id_seat;

        if(tmp[z].booked == false){
          tmpn["basefill"] =  tmp[z].fill;
        } else{
          tmpn["basefill"] =  tmp[z].basefill;
        }

        //console.log("x="+tmp[z].left+" y="+tmp[z].top);

        tmps.push(tmpn);
        tmpn = {};
        //console.log(tmp[z].used);
      }
    }
    data["version"] = ver;
    data["kode"] = kodeevent;
    data["active"] = $("#obj-active").val();
    data["deleted"] = deleted;
    data["objects"] = tmps;

    //console.log(canvas.toJSON());
    dataglobal = JSON.stringify(data);
    callback();

    return JSON.stringify(data);

  }
  function ajaxs(){
    $.ajax({
      url: "../../API/back/canvas",
      type: "PUT",
      data: dataglobal,
      contentType: "application/json",
      success:function(){
        swal(
          'Sukses!',
          'Data berhasil diubah!',
          'success'
        );
        setTimeout(function(){
          window.location.href = "event_view.php?id="+$("#_id").val();
        },1000);
      },
      error:function(x,y,z){
        //console.log(x,y,z);
        swal(
          'Gagal!',
          'Data gagal diubah!',
          'error'
        );
      }
    });
  }

  // Load Canvas DATA from API

  $.ajax({
    url: "../../API/back/canvas2/"+canvasid,
    type: "GET",
    success:function(res){
      ver = res.version;
      kodeevent = res.kode_event;

      json = parseJSON(res);
      $("#obj-active").val(res.active);
    },
    error:function(){

    },
    async: false
  });

  canvas.clear();
  canvas.loadFromJSON(json, function() {
    canvas.renderAll();
  });


  window.addEventListener('resize', resizeCanvas, false);
  resizeCanvas();

  // setGrid(canvas.getWidth(),grid);

  //console.log(JSON.stringify(canvas.getObjects()));


  // Event Trigger

  canvas.on('object:moving', function(options) {
    if (Math.round(options.target.left / grid * 4) % 4 == 0 &&
    Math.round(options.target.top / grid * 4) % 4 == 0) {
      options.target.set({
        left: Math.round(options.target.left / grid) * grid,
        top: Math.round(options.target.top / grid) * grid
      }).setCoords();
    }
    // customsave();
    //console.log(options.target.left," ",options.target.top);
    //console.log(options);
    //console.log(canvas.getWidth()+"x"+canvas.getHeight());
  });

  $(document).keydown(function(e) {
    var temp;

    if (e.keyCode == 46) {
      e.preventDefault();

      // Jika objek hanya satu
      if(canvas.getActiveObject()._objects == undefined){
        var active = canvas.getActiveObject();

        if(active.booked == true){
          swal(
            'Error!',
            'Kursi sudah dipesan!',
            'error'
          )
        } else{
          if(active.id_seat){
            deleted.push(active.id_seat);
          }

          canvas.remove(canvas.getActiveObject());
        }

      } else {
        // Jika Objek lebih dari 1

        var z = canvas.getActiveObject()._objects;

        for(x in z){
          if(z[x].booked !== true){
            if(z[x].id_seat){
              deleted.push(z[x].id_seat);
            }

            canvas.remove(z[x]);
          }
        }
      }
      optionFill();
    }
    // CTRL + C KEY

    if (e.keyCode == 67 && e.ctrlKey) {
      e.preventDefault();
      canvas.getActiveObject().clone(function(cloned) {
        _clipboard = cloned;
      });
    }

    // CTRL + V KEY
    if (e.keyCode == 86 && e.ctrlKey) {
      e.preventDefault();
      _clipboard.clone(function(clonedObj) {
        canvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + grid*2,
          top: clonedObj.top,
          evented: true,
        });
        if (clonedObj.type === 'activeSelection') {

          clonedObj.canvas = canvas;
          clonedObj.forEachObject(function(obj) {
            canvas.add(obj);
          });

          clonedObj.setCoords();
        } else {
          canvas.add(clonedObj);
        }
        _clipboard.top;
        _clipboard.left += grid*2;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
      });
      // optionFill();
    }

  });
  canvas.on('mouse:down',function(e){
    //console.log("down");
    //console.log(canvas.getActiveObject());
    optionFill();
  });

  $("#obj-color").ColorPicker({
    color: "#ff0000",
    onBeforeShow: function(el){
      $(this).ColorPickerSetColor(tmpc);
    },
    onChange: function (hsb, hex, rgb) {
      $(this).val('#' + hex);
      $("#obj-color").val('#'+hex);
      tmpc = '#'+hex;
      $(this).ColorPickerSetColor(tmpc);
      itmcolor('#'+hex);
    },
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      itmcolor("#"+hex);
      $(el).ColorPickerHide();
    }
  });
  $("#obj-active").ColorPicker({
    color: $(this).val(),
    onBeforeShow: function(el){
      $(this).ColorPickerSetColor($(this).val());
    },
    onChange: function (hsb, hex, rgb) {
      $(this).val('#' + hex);
      $("#obj-active").val('#'+hex);
      tmpc = '#'+hex;
      $(this).ColorPickerSetColor(tmpc);

      var z = canvas.getObjects();

      for(x in z){
        if(z[x].booked == true){
          z[x].set("fill",tmpc);
        }
      }
      canvas.renderAll();
    },
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      $(el).ColorPickerHide();
    }
  });

  $("#clear").click(function(){
    canvas.clear();
    clearGrid();

    $.ajax({
      url: "../../API/back/canvas2/"+canvasid,
      type: "GET",
      success:function(res){
        ver = res.version;
        kodeevent = res.kode_event;

        json = parseJSON(res);
        $("#obj-active").val(res.active);
      },
      error:function(){

      },
      async: false
    });

    canvas.loadFromJSON(json, function() {
      canvas.renderAll();
    });
    setGrid(canvas.getWidth(),grid);
  });
  $("#save").click(function(){
    //console.log(deleted);
    customsave(ajaxs);

  });

  var currentlyDragging;

  function handleDragStart(e) {
    //console.log("start",e.srcElement.id);
    type = e.srcElement.id;
    [].forEach.call(images, function (img) {
      img.classList.remove('img_dragging');
    });
    this.classList.add('img_dragging');
    //currentlyDragging = e.target;
  }

  function handleDragOver(e) {
    //console.log(e);
    if (e.preventDefault) {
      e.preventDefault(); // Necessary. Allows us to drop.
    }

    e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
    // NOTE: comment above refers to the article (see top) -natchiketa

    return false;
  }

  function handleDragEnter(e) {
    // this / e.target is the current hover target.
    this.classList.add('over');
    //console.log(e);
  }

  function handleDragLeave(e) {
    this.classList.remove('over'); // this / e.target is previous target element.
  }

  function handleDrop(e) {
    if (e.preventDefault) {

      e.preventDefault();
    }

    if (e.stopPropagation) {
      e.stopPropagation(); // stops the browser from redirecting.
    }

    //var ext = currentlyDragging.src.substr(-3);
    var sx;

    var xv;
    var yv;


    switch(type){
      case "circle":
      sx = makeCircle({
        left: e.layerX,
        top: e.layerY,
        fill: '#0000ff',
        width: grdx,
        height: grdx,
        radius: grdx/2,
        obj : true
      });
      break;

      case "rectx":
      sx = makeRect({
        left: e.layerX,
        top: e.layerY,
        fill: '#ff0000',
        width: grdx,
        height: grdx,
        obj : true
      });
      break;
    }

    var jml = parseInt(prompt("Masukkan jumlah :"));

    if(!isNaN(jml)){
      xv = e.layerX;
      yv = e.layerY;

      for(let z=0;z<jml;z++){
        switch(type){
          case "circle":
          sx = makeCircle({
            left: xv,
            top: yv,
            fill: '#0000ff',
            width: grdx,
            height: grdx,
            radius: grdx/2,
            obj : true
          });
          break;

          case "rectx":
          sx = makeRect({
            left: xv,
            top: yv,
            fill: '#ff0000',
            width: grdx,
            height: grdx,
            obj : true
          });
          break;
        }

        canvas.add(sx);

        xv+=grid*2;
      }
    } else{
      canvas.add(sx);
    }

    canvas.getObjects('line');
    return true;
  }

  function handleDragEnd(e) {
    // this/e.target is the source node.
    [].forEach.call(images, function (img) {
      img.classList.remove('img_dragging');
    });
  }

  if (Modernizr.draganddrop) {
    // Browser supports HTML5 DnD.
    //console.log("drag");
    // Bind the event listeners for the image elements
    var images = $("img");

    var objects = $('#images object');
    [].forEach.call(images, function (img) {
      img.addEventListener('dragstart', handleDragStart, false);
      img.addEventListener('dragend', handleDragEnd, false);
    });
    [].forEach.call(objects, function (obj) {
      obj.addEventListener('dragstart', handleDragStart, false);
      obj.addEventListener('dragend', handleDragEnd, false);
    });
    // Bind the event listeners for the canvas
    var canvasContainer = document.getElementById('box-body');
    canvasContainer.addEventListener('dragenter', handleDragEnter, false);
    canvasContainer.addEventListener('dragover', handleDragOver, false);
    canvasContainer.addEventListener('dragleave', handleDragLeave, false);
    canvasContainer.addEventListener('drop', handleDrop, false);
  } else {
    // Replace with a fallback to a library solution.
    alert("This browser doesn't support the HTML5 Drag and Drop API.");
  }
});
