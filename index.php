<?php
define('INTERNAL',true);

// include "lib/helper.php";

if(!isset($_GET['page'])){
  header("Location: ?page=index");
  die();
}

if($_GET['page'] == ""){
  header("Location: ?page=index");
  die();
}

if($_GET['page'] == "index"){
  include "check.php";
  die();
}

if($_GET['page'] == "step2"){
  include "step-2c.php";
  die();
}

if($_GET['page'] == "step3"){
  include "step-3.php";
  die();
}

if($_GET['page'] == "step4"){
  include "step-4.php";
  die();
}

if($_GET['page'] == "cancel"){
  include "destroy.php";
  die();
}
if($_GET['page'] == "finish"){
  include "destroy.php";
  die();
}

if($_GET['page'] == "confirm"){
  include "confirm.php";
  die();
}

if($_GET['page'] == "getqr"){
  include "getqr.php";
  die();
}

header("HTTP/1.1 301 Moved Permanently");
header("Location: ?page=index");

die();
?>
