<?php
require "../../vendor/autoload.php";
require "../../lib/helper.php";

use dbase\datafunction;

define('GLOBAL',true);
define('PAGE', 'admin@event');
session_start();

$o = new datafunction();
$o->isloggedin("","../");

if(!isset($_GET['idusr'])){
  move("event.php");
}
if(!isset($_GET['idseat'])){
  move("event.php");
}
if(empty($_GET['idusr'])){
  move("event.php");
}
if(empty($_GET['idseat'])){
  move("event.php");
}

if(!$o->chkIfConfirmed($_GET['idusr'],$_GET['idseat'])){
  move("event.php");
}

$data = $o->dataConfirmAdmin($_GET['idusr'],$_GET['idseat']);
$eid = $o->getEventIDBySeatCode($_GET['idusr']);

$kode = $o->kodeEvent($eid);


?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Event Page | Attract!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- DATA TABLES -->

  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" media="screen" type="text/css" href="../../plugin/colorpicker/css/colorpicker.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="../bower_components/colorpicker/css/colorpicker.css">

  <script type="text/javascript" src="../bower_components/jquery/jquery-3.3.1.min.js"></script>


</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "inc/header.inc.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <?php include "inc/sidebar.inc.php"; ?>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Detail Confirm
          <small>Confirm Information</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Event</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-7">
            <div class="box box-primary" style="max-height: 550px;overflow-y:auto">
              <div class="box-header">
                <h3 class="box-title">Detail Tamu</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="Nama">Nama</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-circle"></i></span>
                    <input type="text" class="form-control" placeholder="Nama" value="<?php echo $data['nama']; ?>" disabled>
                  </div>
                  <label for="email">Email</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                    <input type="text" class="form-control" placeholder="Email" value="<?php echo $data['email']; ?>" disabled>
                  </div>
                  <label for="tgl">Tanggal</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="form-control" placeholder="Tanggal" value="<?php echo $data['date']; ?>" disabled>
                  </div>
                  <label for="tgl">Private</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="text" class="form-control" placeholder="Private Seed" value="<?php echo $data['seed']; ?>" disabled>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <!-- right col -->
          <div class="col-lg-5">
            <div class="box box-danger">
              <div class="box-header">
                <h3 class="box-title">Posisi</h3>
              </div>
              <div class="box-body">
                <canvas id="canvas" width="300" height="300"></canvas>
              </div>

            </div>
          </div>
          <div class="col-lg-5 col-lg-offset-7">
            <div class="box box-success">
              <div class="box-header">
                <h3 class="box-title">Kode QR</h3>
              </div>
              <div class="box-body text-center">
                <img src="<?php echo $data['qrdata']; ?>" alt="QRcode Confirm">
              </div>

            </div>
          </div>
        </div>
        <!-- /.row (main row) -->

      </section>

        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.0.1
        </div>
        <strong>Copyright &copy; Attract</a>.</strong> All rights
        reserved.
      </footer>

      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <!--
    <script src="../bower_components/jquery/dist/jquery.min.js"></script> -->

    <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
    <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script type="text/javascript" src="../bower_components/colorpicker/js/colorpicker.js"></script>

    <!-- SweetALERT2 -->
    <script type="text/javascript" src="../bower_components/sweetalert2/dist/sweetalert2.all.min.js"></script>

    <script type="text/javascript" src="../bower_components/chance/dist/chance.min.js"></script>
    <script type="text/javascript" src="../../canvas/js/fabric.min.js"></script>

    <script type="text/javascript" src="../../canvas/js/modernizr.min.js"></script>

    <script type="text/javascript">
    $("document").ready(function(){
      var canvas = new fabric.Canvas('canvas');
      var cs = $("#canvas");

      cs.css("border","1px solid #ccc");

      function resizeCanvas() {
        var bwidth = cs.parent().parent().css("width");
        canvas.setWidth(parseInt(bwidth)-20);
        canvas.renderAll();
      }
      window.addEventListener('resize', resizeCanvas, false);
      resizeCanvas();

      function genid(){
        var unix = moment().format("x");
        var date = moment().format("DDMMYY");
        var rand = chance.integer({min:1000,max:9999});
        var final = "T"+date+rand;
        return final;
      }

      function exportJSON(json){
        if(json.objects.length > 0){
          var tmp = [];

          for(object in json.objects){
            if(json.objects[object].type != "line"){
              json.objects[object].lockRotation  = true;

              json.objects[object].lockScalingY  = true;
              json.objects[object].lockScalingX  = true;

              json.objects[object].lockMovementX = true;
              json.objects[object].lockMovementY = true;

              json.objects[object].hasControls   = false;
              json.objects[object].hasBorders    = false;

              json.objects[object].hoverCursor   = "pointer";

              tmp.push(json.objects[object]);
            }
          }
        }
        var data = {};
        data["version"] = "2.3.4";
        data["objects"] = tmp;

        return data;
      }

      function zoomIt(factor,json) {
        // canvas.setHeight(canvas.getHeight() * factor);
        // canvas.setWidth(canvas.getWidth() * factor);

        // if (canvas.backgroundImage) {
        //   // Need to scale background images as well
        //   var bi = canvas.backgroundImage;
        //   bi.width = bi.width * factor; bi.height = bi.height * factor;
        // }

        canvas.loadFromJSON(json, function() {
          canvas.renderAll();
        });

        var objects = canvas.getObjects();
        //var objects = json.objects;
        // console.log(objects);

        for (var i in objects) {
          if(objects[i].id_seat == <?php echo $_GET['idseat']; ?>){
            objects[i].fill = json.active;
          } else{
            objects[i].fill = "#afafaf";
          }
          var scaleX = objects[i].scaleX;
          var scaleY = objects[i].scaleY;
          var left = objects[i].left;
          var top = objects[i].top;

          // var width = objects[i].width;
          // var height = objects[i].height;

          var tempScaleX = scaleX * factor;
          var tempScaleY = scaleY * factor;
          var tempLeft = left * factor;
          var tempTop = top * factor;

          // var tempWidth = width * factor;
          // var tempHeight = height * factor;

          objects[i].scaleX = tempScaleX;
          objects[i].scaleY = tempScaleY;
          objects[i].left = tempLeft;
          objects[i].top = tempTop;

          objects[i].setCoords();
        }

        json = exportJSON(canvas.toJSON());
        // console.log(JSON.stringify(json));

        canvas.clear();
        canvas.loadFromJSON(json, function() {
          canvas.backgroundColor = "#dedede";
          canvas.renderAll();
          canvas.selection = false;
        });
      }

      $("#regenerate").click(function(){
        $("input[name=evkode]").val(genid());
      });

      $("input[name=evkode]").val(genid());
      $(".sidebar-toggle").click(function(){
        setTimeout(function(){
          resizeCanvas();
        },500);
      });

      $.ajax({
        url: "../../API/back/canvas2/<?php echo $kode; ?>",
        type: "GET",
        success:function(res){
          zoomIt(0.45,res);
        },
        error:function(x,y,z){
          console.log(x,y,z);
        }
      });
    });
    </script>
  </body>
  </html>
