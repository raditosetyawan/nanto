<?php
require "../../vendor/autoload.php";
require "../../lib/helper.php";

use dbase\datafunction;

define('GLOBAL',true);
define('PAGE', 'admin@seat');
session_start();

$o = new datafunction();
$o->isloggedin("","../");

if(isset($_GET['id'])){
  $kode = $o->kodeEvent($_GET['id']);
  if(empty($_GET['id'])){
    $o->redirect("event.php");
    exit(0);
  } else{
    if($o->canvasExist($_GET['id'])){
      $o->redirect("event_seat_edit.php?id=".$_GET['id']);
      exit(0);
    }
  }
} else{
  $o->redirect("event.php");
  exit(0);
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Draw Seat | Tamugo!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" media="screen" type="text/css" href="../../plugin/colorpicker/css/colorpicker.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="../bower_components/colorpicker/css/colorpicker.css">

  <script type="text/javascript" src="../../js/jquery-3.3.1.min.js"></script>


</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "inc/header.inc.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <?php include "inc/sidebar.inc.php"; ?>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Seats Editor
          <small><?php echo $kode; ?></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Seats</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-2">
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Shape</h3>
              </div>
              <!-- /.box-header -->
              <style media="screen">
              #images img{
                width: 30px;
                margin-right: 5px;
              }
              </style>
              <div class="box-body">
                <div id="images" class="items">
                  <img draggable="true" id="rectx" src="../../canvas/img/rblue.svg" alt="rectblue">
                  <img draggable="true" id="circle" src="../../canvas/img/cred.svg" alt="circlered">
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Option</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="form-group">
                  <label for="color">Color</label>
                  <input type="text" id="obj-color" name="" value="" class="form-control">
                </div>
                <div class="form-group">
                  <label for="color">Active Color</label>
                  <input type="text" id="obj-active" name="" value="#000" class="form-control">
                </div>
                <div class="form-group">
                  <label for="color">Width / Height</label>
                  <input type="text" id="obj-wh" name="widths" value="" class="form-control">
                </div>

                <div class="form-group">
                  <label for="color">X coord / Y coord</label>
                  <input type="text" id="obj-coord" name="heights" value="" class="form-control">
                </div>

                <input type="hidden" name="_id" id="_id" value="<?php echo $_GET['id']?>" disabled>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <button type="button" id="save" class="btn btn-md btn-primary" name="button"><i class="fa fa-save"></i> Save</button>
            <button type="button" id="clear" class="btn btn-md btn-danger" name="button"><i class="fa fa-undo"></i> Reset</button>
          </section>
          <section class="col-lg-10">
            <div class="box box-primay">
              <div class="box-header">
                <h3 class="box-title">Editor</h3>
              </div>
              <!-- /.box-header -->
              <script>
              $("document").ready(function(){
              });
              </script>
              <style media="screen">
              canvas{
                eborder: 1px solid #ccc;
                box-shadow: inset 0 1px 4px #ccc;
              }
              </style>
              <div class="box-body" id="box-body">
                <canvas id="c"></canvas>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
      </div>
      <strong>Copyright &copy; Tamugo</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../bower_components/raphael/raphael.min.js"></script>
<script src="../bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- SweetALERT2  -->
<script src="../bower_components/sweetalert2/dist/sweetalert2.all.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script type="text/javascript" src="../bower_components/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="../../canvas/js/fabric.min.js"></script>
<script type="text/javascript" src="../../canvas/js/main2.js"></script>
<script type="text/javascript" src="../../canvas/js/modernizr.min.js"></script>
</body>
</html>
