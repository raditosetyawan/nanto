<?php
require "../../vendor/autoload.php";
require "../../lib/helper.php";

use dbase\datafunction;

define('GLOBAL',true);
define('PAGE', 'admin@users');
session_start();

$o = new datafunction();
$o->isloggedin("","../");

if($_SESSION['role'] != "master"){
  $o->redirect("./");
  die();
}
if(!isset($_GET['id'])){
  $o->redirect("admin.php");
  die();
}

if(isset($_POST['foo'])){
  $o->updAdmin($_POST);

  header("Location: admin.php");
  die();
}

$data = $o->getAdmin($_GET['id']);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Master Page | Tamugo!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- DATA TABLES -->

  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" media="screen" type="text/css" href="../../plugin/colorpicker/css/colorpicker.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="../bower_components/colorpicker/css/colorpicker.css">

  <script type="text/javascript" src="../bower_components/jquery/jquery-3.3.1.min.js"></script>


</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "inc/header.inc.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <?php include "inc/sidebar.inc.php"; ?>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Admin List
          <small>Master Only</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Master</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Daftar Admin</h3>
              </div>
              <div class="box-body">
                <form action="" method="post">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" placeholder="Nama Admin" class="form-control" value="<?php echo $data['nama']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="uname">Username</label>
                    <input type="text" name="uname" placeholder="Username" class="form-control" value="<?php echo $data['username']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="passw">Password</label>
                    <input type="password" name="passw" placeholder="Password" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control" name="role">
                      <option value=""></option>
                      <option value="0" <?php if($data['role'] == "master") echo "selected"; ?>>Master</option>
                      <option value="1" <?php if($data['role'] == "admin") echo "selected"; ?>>Admin</option>
                    </select>
                  </div>
                  <input type="hidden" name="foo" value="<?php echo $_GET['id']; ?>">

                  <button type="submit" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

              </div>
            </div>
            <!-- right col -->
          </div>
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
          <div class="pull-right hidden-xs">
            <b>Version</b> 0.0.1
          </div>
          <strong>Copyright &copy; Tamugo</a>.</strong> All rights
          reserved.
        </footer>

        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
      </div>
      <!-- ./wrapper -->
      <!--
      <script src="../bower_components/jquery/dist/jquery.min.js"></script> -->

      <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
      <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
      <!-- jQuery UI 1.11.4 -->
      <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
      <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
      <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/../pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script type="text/javascript" src="../bower_components/colorpicker/js/colorpicker.js"></script>

    <script type="text/javascript" src="../../canvas/js/modernizr.min.js"></script>

    <script type="text/javascript">
    $("document").ready(function(){
      // $('#tblevent').DataTable({
      //   "processing": true,
      //   'paging'      : true,
      //   'lengthChange': true,
      //   'searching'   : true,
      //   'ordering'    : true,
      //   'info'        : true,
      //   'autoWidth'   : false
      // });
      //
      $(".prog").each(function() {
        var pc = $(this).text();
        let x;

        if(pc == "master"){
          x='bg-red';
        } else if(pc == "admin"){
          x='bg-blue'
        }

        $(this).addClass("badge");
        $(this).addClass(x);
      });

    });
  </script>
</body>
</html>
<?php
print_r($_POST);
?>
