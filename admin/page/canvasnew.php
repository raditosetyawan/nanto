<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>FABRIC JS From Canvas</title>
  <script type="text/javascript" src="../../canvas/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="../../canvas/js/fabric.min.js"></script>
  <script type="text/javascript" src="../../admin/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
  <script type="text/javascript">
  $("document").ready(function(){
    var canvas = new fabric.Canvas("canvas");

    function exports(origin,canvas){
      let tmp = JSON.parse(origin);
      let active = tmp.active;
      let object = {};

      object["version"] = tmp.version;
      object["active"] = active;
      object["objects"] = canvas.getObjects();

      return object;
    }
    function update(canvas){
      var tmp = canvas.toJSON();

      canvas.clear();

      canvas.loadFromJSON(tmp, function() {
        canvas.renderAll();
      });
    }


    canvas.loadFromJSON(json, function() {
      canvas.renderAll();
    });
    canvas.on('object:scaling',function(e){
      //e.lockUniScaling = true;
    });
    canvas.on('object:moving', function(e) {
    });
    canvas.on('mouse:down', function(e) {
      if(e.target !== null){
        if(e.target.used == false){
          var tmp = exports(json,canvas);
          var active = JSON.parse(json).active;

          canvas.getActiveObject().set("fill",active);
          canvas.getActiveObject().set("used",true);

          //console.log("active",canvas.getActiveObject());

        } else{
          alert("used");
        }
      }
    });

    canvas.selection = false;
  });
  </script>
</head>
<body>
  <canvas id="canvas" width="1000" height="1000" style="border: 1px solid #000"></canvas>
</body>
</html>
