<?php
require "../../vendor/autoload.php";
require "../../lib/helper.php";

use dbase\datafunction;

define('GLOBAL',true);
define('PAGE', 'admin@event@list');
session_start();

$o = new datafunction();
$o->isloggedin("","../");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Event Page | Tamugo!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- DATA TABLES -->

  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" media="screen" type="text/css" href="../../plugin/colorpicker/css/colorpicker.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="../bower_components/colorpicker/css/colorpicker.css">

  <script type="text/javascript" src="../bower_components/jquery/jquery-3.3.1.min.js"></script>


</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "inc/header.inc.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <?php include "inc/sidebar.inc.php"; ?>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Event List
          <small>Tambah Event</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Event</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Daftar Event</h3>
              </div>
              <div class="box-body">
                <table class="table table-bordered table-hover" id="tblevent">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode</th>
                      <th>Nama</th>
                      <th>Alamat</th>
                      <th>Contact</th>
                      <th>Progress</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Kapasitas</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $datas = $o->dataEvent();
                    $no=1;
                    foreach($datas as $data){
                      $datetime = explode(" ",$data['date']);
                      
                      echo "<tr>
                      <td>$no</td>
                      <td>".$data['kode_event']."</td>
                      <td>".$data['nama_event']."</td>
                      <td>".$data['alamat_event']."</td>
                      <td>".$data['contact_event']."</td>
                      <td><center><span class=\"prog\">".$o->eventProgress($data['id'])."</span></center></td>
                      <td>".$datetime[0]."</td>
                      <td>".$datetime[1]."</td>
                      <td><center>".$data['capacity_event']."</center></td>
                      <td>
                      <center>
                      <a href=\"event_view.php?id=".$data['id']."\" class=\"btn btn-xs btn-primary\"><i class=\"fa fa-eye\"></i></a>
                      <a href=\"event_edit.php?id=".$data['id']."\" class=\"btn btn-xs btn-warning\"><i class=\"fa fa-pencil\"></i></a>
                      <a href=\"action/eventdel.php?id=".$data['id']."\" class=\"btn btn-xs btn-danger evdels\"><i class=\"fa fa-trash\"></i></a>
                      </center>
                      </td>
                      ";
                      $no++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
      </div>
      <strong>Copyright &copy; Tamugo</a>.</strong> All rights
      reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
  <!--
  <script src="../bower_components/jquery/dist/jquery.min.js"></script> -->

  <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
  <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../bower_components/raphael/raphael.min.js"></script>
<script src="../bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script type="text/javascript" src="../bower_components/colorpicker/js/colorpicker.js"></script>

<!-- SweetALERT2 -->
<script type="text/javascript" src="../bower_components/sweetalert2/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript" src="../../canvas/js/modernizr.min.js"></script>

<script type="text/javascript">
$("document").ready(function(){
  $('#tblevent').DataTable({
    "processing"  : true,
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false,
    'fnDrawCallback': function(){

      $(".prog").each(function() {
        var pc = parseInt($(this).text());
        let x;

        if(pc <= 15){
          x='bg-red';
        } else if(pc <= 70){
          x='bg-yellow';
        } else if(pc <= 89){
          x='bg-blue';
        } else if (pc <= 100){
          x='bg-green';
        } else{
          x='bg-red';
        }

        $(this).addClass("badge");
        $(this).addClass(x);
        $(this).text(pc+" %");
      });


        $(".evdels").click(function(e){
          e.preventDefault();

          const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
          })

          swalWithBootstrapButtons({
            title: 'Apakah anda yakin?',
            text: "Data yang terhapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Batal hapus !',
            reverseButtons: true

          }).then((result) => {
            if (result.value) {
              window.location.href = $(this).attr("href");
            } else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons(
                'Dibatalkan',
                'Data tidak jadi dihapus',
                'error'
              )
            }
          })
        });

    }
  });

  $(".prog").each(function() {
    var pc = parseInt($(this).text());
    let x;

    if(pc <= 15){
      x='bg-red';
    } else if(pc <= 70){
      x='bg-yellow';
    } else if(pc <= 89){
      x='bg-blue';
    } else if (pc <= 100){
      x='bg-green';
    } else{
      x='bg-red';
    }

    $(this).addClass("badge");
    $(this).addClass(x);
    $(this).text(pc+" %");
  });

  $(".evdels").click(function(e){
    e.preventDefault();

    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    })

    swalWithBootstrapButtons({
      title: 'Apakah anda yakin?',
      text: "Data yang terhapus tidak dapat dikembalikan!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, Hapus',
      cancelButtonText: 'Batal hapus !',
      reverseButtons: true

    }).then((result) => {
      if (result.value) {
        window.location.href = $(this).attr("href");
      } else if (
        result.dismiss === swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons(
          'Dibatalkan',
          'Data tidak jadi dihapus',
          'error'
        )
      }
    })
  });

});
</script>
</body>
</html>
