<?php
require "../../../vendor/autoload.php";
require "../../../lib/helper.php";

use dbase\datafunction;

session_start();

$o = new datafunction();
$o->isloggedin("","../");
if(isset($_POST['_xsrf'])){
  $data = [
    "nama"=> $_POST['nama'],
    "email"=> $_POST['email'],
    "idevent"=> $_POST['_eventid'],
  ];
  if($o->addTamu($data)){
    $o->redirect("../event_view.php?id=".$_POST['_eventid']);
  } else{
    $o->redirect("../event_view.php?id=".$_POST['_eventid']);
  }
} else{
  $o->redirect("../");
}
?>
