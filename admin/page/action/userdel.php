<?php
require "../../../vendor/autoload.php";
require "../../../lib/helper.php";

use dbase\datafunction;

session_start();

$o = new datafunction();
$o->isloggedin("","../");


if(!isset($_GET['idusr']) AND !isset($_GET['idevent'])){
  move("../event.php");
}

if(!$o->userExist($_GET['idusr'],$_GET['idevent'])){
  move("../event.php");
}

$o->deleteUser($_GET['idusr']);

move("../event_view.php?id=".$_GET['idevent']);
?>
