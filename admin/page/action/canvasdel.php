<?php
require "../../../vendor/autoload.php";
require "../../../lib/helper.php";

use dbase\datafunction;

session_start();

$o = new datafunction();
$o->isloggedin("","../");


if(isset($_GET['idevent'])){
  $o->deleteCanvas($_GET['idevent']);

  move("../event_view.php?id=".$_GET['idevent']);
} else{
  move("../");
}
?>
