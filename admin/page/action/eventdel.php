<?php
require "../../../vendor/autoload.php";
require "../../../lib/helper.php";

use dbase\datafunction;

session_start();

$o = new datafunction();
$o->isloggedin("","../");


if(isset($_GET['id'])){
  $o->deleteEvent($_GET['id']);

  move("../event.php");
} else{
  move("../");
}
?>
