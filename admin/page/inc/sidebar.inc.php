<?php
if(!defined('GLOBAL')){
  exit(0);
}
$ses = (object) $_SESSION;

?>

<!-- Sidebar user panel -->
<div class="user-panel">
  <div class="pull-left image">
    <img src="../dist/img/shroud.png" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p><?php echo $ses->name ?></p>
    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
  </div>
</div>

<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?php page("admin@index"); ?>">
    <a href="./">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
  </li>
  <li class="treeview <?php page("admin@event"); page("admin@event@list"); page("admin@event@add"); ?>">
    <a href="#">
      <i class="fa fa-book"></i> <span>Event</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="<?php page("admin@event@list"); ?>"><a href="event.php"><i class="fa fa-circle-o"></i> Kelola Event</a></li>
      <li class="<?php page("admin@event@add"); ?>"><a href="event_add.php"><i class="fa fa-circle-o"></i> Tambah Event</a></li>
    </ul>
  </li>
  <?php
  if($ses->role == "master"){
    ?>
    <li class="<?php page("admin@users"); ?>">
      <a href="admin.php">
        <i class="fa fa-key"></i> <span>Administrator</span>
      </a>
    </li>
    <?php
  }
  ?>
</ul>
