<?php
if(!defined('GLOBAL')){
  exit(0);
}
$ses = (object) $_SESSION;
?>

<!-- Logo -->
<a href="./index.php" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>A</b>T!</span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>Attract</b>!</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="../dist/img/shroud.png" class="user-image" alt="User Image">
          <span class="hidden-xs"><?php echo $ses->name; ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="../dist/img/shroud.png" class="img-circle" alt="User Image">

            <p>
              <?php echo $ses->name; ?>
              <small>Web Administrator</small>
            </p>
          </li>

          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-right">
              <a href="../logout.php" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
      <!-- Control Sidebar Toggle Button -->
    </ul>
  </div>
</nav>
