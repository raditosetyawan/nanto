<?php
require "../../vendor/autoload.php";
require "../../lib/helper.php";

use dbase\datafunction;

define('GLOBAL',true);
define('PAGE', 'admin@event');
session_start();

$o = new datafunction();
$o->isloggedin("","../");

if(isset($_GET['id'])){

  if(empty($_GET['id'])){
    $o->redirect("event.php");
    exit(0);
  }

  $code = $o->kodeEvent($_GET['id']);

} else{
  $o->redirect("event.php");
  exit(0);
}

$guests = $o->dataTamu($_GET['id']);
//print_r($guests);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Event Page | Tamugo!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- DATA TABLES -->

  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" media="screen" type="text/css" href="../../plugin/colorpicker/css/colorpicker.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="../bower_components/colorpicker/css/colorpicker.css">

  <script type="text/javascript" src="../bower_components/jquery/jquery-3.3.1.min.js"></script>


</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "inc/header.inc.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <?php include "inc/sidebar.inc.php"; ?>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Event View
          <small><?php echo $code; ?></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Event</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-7">
            <div class="box box-primary" style="max-height: 550px;overflow-y:auto">
              <div class="box-header">
                <h3 class="box-title">Data Tamu</h3>
              </div>
              <div class="box-body">
                <div style="margin-bottom: 5px">
                  <a class="btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i> Tambah</a>
                </div>
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Konfirmasi</th>
                      <th><center>Aksi</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $n=1;
                    // print_r($guests);

                    foreach($guests as $guest){
                      // $guest['booked'];

                      $z = $guest['booked'] == "1" ? '<span class="badge bg-green">SUDAH</span>' : '<span class="badge bg-red">BELUM</span>';
                      $qr = $guest['booked'] !== "1" ? 'disabled' : '';
                      $hr = $guest['booked'] !== "1" ? '' : 'href="confirm_detail.php?idusr='.$guest['id'].'&idseat='.$guest['idseat'].'"';


                      echo '
                      <td>'.$n.'</td>
                      <td>'.$guest['nama'].'</td>
                      <td>'.$guest['email'].'</td>
                      <td>'.$z.'</td>
                      <td>
                      <center>
                      <a class="btn btn-xs btn-primary '.$qr.'" '.$hr.'><i class="fa fa-qrcode"></i></a>
                      <a class="btn btn-xs btn-warning" href="user_edit.php?id='.$guest['id'].'&eid='.$_GET['id'].'"><i class="fa fa-pencil"></i></a>
                      <a class="btn btn-xs btn-danger delete" href="action/userdel.php?idusr='.$guest['id'].'&idevent='.$_GET['id'].'"><i class="fa fa-trash"></i></a>
                      </center>
                      </td>
                      </tr>';
                      $n++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
          <!-- right col -->
          <div class="col-lg-5">
            <div class="box box-danger">
              <div class="box-header">
                <h3 class="box-title">Layout</h3>
              </div>
              <div class="box-body">
                <div class="containes" style="margin-bottom: 5px;">
                  <div class="pull-left">
                    <a class="btn btn-primary" href="event_seat.php?id=<?php echo $_GET['id']; ?>"><i class="fa fa-pencil"></i> Ubah</a>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-danger" id="canvasdel"><i class="fa fa-trash"></i> Hapus</a>
                  </div>
                </div>
                <br>
                <br>
                <div class="fuu">
                  <canvas id="canvas" width="300" height="300"></canvas>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- /.row (main row) -->

      </section>
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Tamu</h4>
              </div>
              <div class="modal-body">
                <form class="" action="action/tamuadd.php" method="post">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" placeholder="Nama Tamu" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="nama">Email</label>
                    <input type="email" name="email" placeholder="Email Tamu" class="form-control">
                  </div>
                  <input type="hidden" name="_eventid" value="<?php echo $_GET['id']; ?>">
                  <input type="hidden" name="_xsrf" value="false">

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.0.1
        </div>
        <strong>Copyright &copy; Tamugo</a>.</strong> All rights
        reserved.
      </footer>

      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <!--
    <script src="../bower_components/jquery/dist/jquery.min.js"></script> -->

    <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
    <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script type="text/javascript" src="../bower_components/colorpicker/js/colorpicker.js"></script>

    <!-- SweetALERT2 -->
    <script type="text/javascript" src="../bower_components/sweetalert2/dist/sweetalert2.all.min.js"></script>

    <script type="text/javascript" src="../bower_components/chance/dist/chance.min.js"></script>
    <script type="text/javascript" src="../../canvas/js/fabric.min.js"></script>

    <script type="text/javascript" src="../../canvas/js/modernizr.min.js"></script>

    <script type="text/javascript">
    $("document").ready(function(){
      var canvas = new fabric.Canvas('canvas');
      var cs = $("#canvas");

      cs.css("border","1px solid #ccc");

      function resizeCanvas() {
        var bwidth = cs.parent().parent().css("width");
        canvas.setWidth(parseInt(bwidth)+5);
        canvas.renderAll();
      }
      window.addEventListener('resize', resizeCanvas, false);
      resizeCanvas();

      function genid(){
        var unix = moment().format("x");
        var date = moment().format("DDMMYY");
        var rand = chance.integer({min:1000,max:9999});
        var final = "T"+date+rand;
        return final;
      }

      function exportJSON(json){
        if(json.objects.length > 0){
          var tmp = [];

          for(object in json.objects){
            if(json.objects[object].type != "line"){
              json.objects[object].lockRotation  = true;

              json.objects[object].lockScalingY  = true;
              json.objects[object].lockScalingX  = true;

              json.objects[object].lockMovementX = true;
              json.objects[object].lockMovementY = true;

              json.objects[object].hasControls   = false;
              json.objects[object].hasBorders    = false;

              json.objects[object].hoverCursor   = "pointer";

              tmp.push(json.objects[object]);
            }
          }
        }
        var data = {};
        data["version"] = "2.3.4";
        data["objects"] = tmp;

        return data;
      }

      function zoomIt(factor,json) {
        // canvas.setHeight(canvas.getHeight() * factor);
        // canvas.setWidth(canvas.getWidth() * factor);

        // if (canvas.backgroundImage) {
        //   // Need to scale background images as well
        //   var bi = canvas.backgroundImage;
        //   bi.width = bi.width * factor; bi.height = bi.height * factor;
        // }

        canvas.loadFromJSON(json, function() {
          canvas.renderAll();
        });

        var objects = canvas.getObjects();
        //var objects = json.objects;

        for (var i in objects) {
          var scaleX = objects[i].scaleX;
          var scaleY = objects[i].scaleY;
          var left = objects[i].left;
          var top = objects[i].top;

          // var width = objects[i].width;
          // var height = objects[i].height;

          var tempScaleX = scaleX * factor;
          var tempScaleY = scaleY * factor;
          var tempLeft = left * factor;
          var tempTop = top * factor;

          // var tempWidth = width * factor;
          // var tempHeight = height * factor;

          objects[i].scaleX = tempScaleX;
          objects[i].scaleY = tempScaleY;
          objects[i].left = tempLeft;
          objects[i].top = tempTop;

          objects[i].setCoords();
        }

        json = exportJSON(canvas.toJSON());
        // console.log(JSON.stringify(json));

        canvas.clear();
        canvas.loadFromJSON(json, function() {
          canvas.backgroundColor = "#dedede";
          canvas.renderAll();
          canvas.selection = false;
        });
      }

      $("#regenerate").click(function(){
        $("input[name=evkode]").val(genid());
      });

      $("input[name=evkode]").val(genid());
      $(".sidebar-toggle").click(function(){
        setTimeout(function(){
          resizeCanvas();
        },500);
      });

      $.ajax({
        url: "../../API/back/canvas/<?php echo $_GET['id']; ?>",
        type: "GET",
        success:function(res){

          zoomIt(0.6,res);
        },
        error:function(x,y,z){
          console.log(x,y,z);
        }
      });

      $(".delete").click(function(e){
        e.preventDefault();
        let href = $(this).attr("href");

        const swalWithBootstrapButtons = swal.mixin({
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false,
        })

        swalWithBootstrapButtons({
          title: 'Apakah anda yakin?',
          text: "Data yang terhapus tidak dapat dikembalikan!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Ya, Hapus',
          cancelButtonText: 'Batal hapus !',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            window.location.href = href;

          } else if (
            result.dismiss === swal.DismissReason.cancel
          ) {
            swalWithBootstrapButtons(
              'Dibatalkan',
              'Data tidak jadi dihapus',
              'error'
            )
          }
        })

      });

      $("#canvasdel").click(function(){
        const swalWithBootstrapButtons = swal.mixin({
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false,
        })

        swalWithBootstrapButtons({
          title: 'Apakah anda yakin?',
          text: "Data yang terhapus tidak dapat dikembalikan!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Ya, Hapus',
          cancelButtonText: 'Batal hapus !',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            window.location.href="action/canvasdel.php?idevent=<?php echo $_GET['id']; ?>";
          } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
          ) {
            swalWithBootstrapButtons(
              'Dibatalkan',
              'Data tidak jadi dihapus',
              'error'
            )
          }
        })
      });

    });
    </script>
  </body>
  </html>



  <?php
  if(isset($_POST['submit'])){
    $data = [
      "kode" => $o->post('evkode'),
      "nama" => $o->post('evnama'),
      "alamat" => $o->post('evalamat'),
      "kontak" => $o->post('evkontak'),
      "kapasitas" => $o->post('evkapasitas')
    ];

    $o->addevent($data);
  }
  ?>
