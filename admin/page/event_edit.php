<?php
require "../../vendor/autoload.php";
require "../../lib/helper.php";

use dbase\datafunction;

define('GLOBAL',true);
define('PAGE', 'admin@event');
session_start();

$o = new datafunction();
$o->isloggedin("","../");


if(!isset($_GET['id'])){
  move("event.php");
}
$data = $o->dataEvent($_GET['id']);
$data = $data[0];

$date = explode(" ",$data['date']);

if(isset($_POST['submit'])){
  $o->eventEdit($_POST);

  $o->movePage("event.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Event Page | Tamugo!</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- DATA TABLES -->

  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" type="text/css" href="../../plugin/colorpicker/css/colorpicker.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="../bower_components/colorpicker/css/colorpicker.css">

  <!-- Timepicker -->
  <link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.min.css">

  <script type="text/javascript" src="../bower_components/jquery/jquery-3.3.1.min.js"></script>


</head>
<body class="hold-transition skin-purple sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "inc/header.inc.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <?php include "inc/sidebar.inc.php"; ?>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Event List
          <small>Edit Event</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Event</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="box box-primary with-border">
              <div class="box-header">
                <h3 class="box-title">Edit Event</h3>
              </div>
              <div class="box-body">
                <form class="form form-group" action="" method="post">
                  <div class="form-group">
                    <label for="code">Kode</label>
                      <input type="text" name="evkode" placeholder="Kode Event" class="form-control" value="<?php echo $data['kode_event']; ?>" readonly>
                  </div>

                  <div class="form-group">
                    <label for="evnama">Nama</label>
                    <input type="text" class="form-control" name="evnama" placeholder="Nama Event" value="<?php echo $data['nama_event']; ?>" required>
                  </div>

                  <div class="form-group">
                    <label for="evalamat">Alamat</label>
                    <input type="text" class="form-control" name="evalamat" placeholder="Alamat Event" value="<?php echo $data['alamat_event']; ?>" required>
                  </div>

                  <div class="form-group">
                    <label for="evkontak">Kontak</label>
                    <input type="text" class="form-control" name="evkontak" placeholder="Kontak Event" value="<?php echo $data['contact_event']; ?>" required>
                  </div>

                  <div class="form-group">
                    <label for="evkapasitas">Kapasitas</label>
                    <input type="number" class="form-control" name="evkapasitas" placeholder="Kapasitas Event" value="<?php echo $data['capacity_event']; ?>" required>
                  </div>

                  <div class="form-group">
                    <label for="evtgl">Tanggal</label>
                    <input type="text" class="form-control" name="evtgl" id="datepicker" placeholder="Tanggal Event" value="<?php echo $date[0]; ?>" required>
                  </div>

                  <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label for="evjam">Jam</label>
                      <input type="text" class="form-control timepicker" name="evjam" placeholder="Jam Event" value="<?php echo $date[1]; ?>" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <input type="submit" name="submit" value="Kirim" class="btn btn-primary pull-left">
                    <input type="reset" name="reset" value="Reset" class="btn btn-danger pull-right">
                  </div>

                </form>
              </div>

            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
      </div>
      <strong>Copyright &copy; Tamugo</a>.</strong> All rights
      reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
  <!--
  <script src="../bower_components/jquery/dist/jquery.min.js"></script> -->

  <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
  <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../bower_components/raphael/raphael.min.js"></script>
<script src="../bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script type="text/javascript" src="../bower_components/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="../bower_components/chance/dist/chance.min.js"></script>
<script type="text/javascript" src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script type="text/javascript" src="../../canvas/js/modernizr.min.js"></script>

<script type="text/javascript">
$("document").ready(function(){
  function genid(){
    var unix = moment().format("x");
    var date = moment().format("DDMMYY");
    var rand = chance.integer({min:1000,max:9999});
    var final = "T"+date+rand;
    return final;
  }
  $('#datepicker').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
  });
  $(".timepicker").keyup(function(e){
    $(this).val("");
  });
  $(".timepicker").keydown(function(e){
    $(this).val("");
  });

  $('.timepicker').timepicker({
    maxHours: 24,
    showMeridian: true,
    showSeconds: false,
    defaultTime: "current",
    showInputs: false,
    minuteStep: 5
  });

  //
  // $("#regenerate").click(function(){
  //   $("input[name=evkode]").val(genid());
  // });

  // $("input[name=evkode]").val(genid());
});
</script>
</body>
</html>
