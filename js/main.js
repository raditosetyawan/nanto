$("document").ready(function(){
	$("#nextbtn").attr("disabled");

	$("#frmvalid").submit(function(e){
		e.preventDefault();

		var frm = $(this).serialize();
		var res;

		if(frm.indexOf('=&') > -1 || frm.substr(frm.length - 1) == '='){
			validation('default');
			return false;
		}

		$.ajax({
			url: 'API/front/cek',
			type: 'POST',
			data: frm,

			success:function(res){
				// console.log(res);
				if(res.status == 'success'){
					validation('1');
					$("#confirm").val("true");
				} else if(res.status == 'error'){
					validation('2');
					$("#confirm").val("false");
				} else if(res.status == 'exist'){
					validation('3');
					$("#confirm").val("false");
				}
			},
			error:function(q,z,v){
				// console.log(q,z);
				validation('2');
				$("#confirm").val("false");
			}
		});
		// console.log();
	});
	$("#confirm").change(function(){
		// console.log("change");
	});
	function validation(z){
		switch(z){
			case '1':
			var htm = '<div class="col-lg-12 text-center"><img src="img/ok.png" height="100px"/><h5>Undangan Valid</h5><p>Undangan anda valid dan ada di dalam sistem kami, mohon segera konfirmasi dan mengambil kode QR acara.</p><p class="none">Undangan anda tidak valid dan tidak ada di dalam sistem kami, mohon hubungi penyelenggara acara anda.</p></div>';
			break;

			case '2':
			var htm = '<div class="col-lg-12 text-center"><img src="img/notok.png" height="100px"/><h5>Undangan Tidak Valid</h5><p>Undangan anda tidak valid dan tidak ada di dalam sistem kami, mohon segera hubungi penyelenggara acara anda.</p></div>';
			break;

			case '3':
			var htm = '<div class="col-lg-12 text-center"><img src="img/confirm.svg" height="150px"/><h5>Undangan Sudah Dikonfirmasi</h5><p>Maaf undangan anda sudah dikonfirmasi, Anda tidak bisa mengkonfirmasi kembali.</p><br><p>Ambil kode QR anda <a href="?page=getqr">disini</a></p></div>';
			break;

			default:
			var htm = '<div class="col-lg-12 text-center"><img src="img/cek.png" height="100px"/><h5>Cek Undangan</h5><p>Cek Undangan untuk mengetahui status undangan anda.</p></div>';
			break;
		}

		$("#rowstat").html(htm);
	}
	$("#nextverify").click(function(e){
		e.preventDefault();
		if($("#confirm").val() == "false"){
			swal({
				type: 'error',
				title: 'Error',
				text: 'Maaf pastikan undangan anda valid!',
			})
		} else{
			window.location.href = $(this).attr("href");
		}
	});

});
