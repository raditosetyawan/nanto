<?php
if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}
session_start();

//require "pages/page1.php";
// print_r($_SESSION);

if(isset($_SESSION['client']['step'])){
  $s = $_SESSION['client']['step']+1;
  header("Location: ?page=step".$s);
  die();
}

?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="package/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <title>Tamugo!</title>
</head>
<body>
  <div class="header">
    <nav class="navbar navbar-light bg-light shadow-sm">
      <a class="navbar-brand"><img src="img/icon.png" width="100px"></a>
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0 base" type="submit">Search</button>
      </form>
    </nav>
  </div>
  <div class="shadow-sm w-80 center">
    <div class="row contain">
      <div class="col-lg-12">
        <div class="row bs-wizard" style="border-bottom:0;">
          <div class="col-xs-3 bs-wizard-step active">
            <div class="text-center bs-wizard-stepnum"><b>Langkah 1</b></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center textactive">Konfirmasi Kehadiran</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 2</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Pilih Kursi / Tempat Duduk Anda</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 3</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cek Ulang Data</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Step 4</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cetak Kode QR</div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
          <div class="col-lg-7">
            <h4>RSVP Undangan</h4>
            <div class="row" style="margin-top: 2rem;">
              <div class="col-lg-12">
                <form id="frmvalid">
                 <label>Kode Undangan</label>
                 <input type="text" name="kode" placeholder="KD92ACH2188" class="form-control">
                 <label>Nama</label>
                 <input type="text" name="nama" placeholder="Nama Anda" class="form-control">
                 <label>Email</label>
                 <input type="text" name="email" placeholder="Email Anda" class="form-control">
                 <input type="hidden" name="_confirm" value="false" id="confirm">
                 <button type="submit" class="btnck" style="margin-top: 1rem">Cek</button>
               </form>
             </div>
           </div>
         </div>
         <div class="col-lg-5">
           <div class="col-lg-12">
             <h4>Status Undangan</h4>
            <div class="row" style="margin-top: 2rem;" id="rowstat">
              <div class="col-lg-12 text-center">
                <img src="img/cek.png" height="100px"/>
                <h5>Cek Undangan</h5>
                <p>Cek Undangan untuk mengetahui status undangan anda.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
          <div class="col-lg-12" style="text-align:right;">
           <a href="?page=step2" id="nextverify"><button class="btnext">Next</button></a>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

</body>
</html>
