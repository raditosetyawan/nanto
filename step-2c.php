<?php
include "lib/helper.php";

if(!defined('INTERNAL')){
  header("HTTP/1.1 400 Bad Request");
  die("You're not supposed to be here!");
}

session_start();

if(!isset($_SESSION['client']['prebook'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['kode'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['nama'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['email'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['idusers'])){
  move("?page=index");
}

if(!isset($_SESSION['client']['step'])){
  move("?page=index");
}

if(isset($_SESSION['client']['step'])){
  if($_SESSION['client']['step'] == 2){
    move("?page=step3");
  } else if($_SESSION['client']['step'] == 3){
    move("?page=step4");
  }
} else{
  move("?page=index");
}

$kode = $_SESSION['client']['kode'];

if(isset($_POST['_seatid'])){
  if(!empty($_POST['_seatid'])){
    $_SESSION['client']['seat'] = $_POST['_seatid'];
    $_SESSION['client']['step'] = 2;

    move("?page=step3");
  }
}

//require "pages/page1.php";
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- <script src="https://unpkg.com/ionicons@4.2.4/dist/ionicons.js"></script> -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript">
  /*
  $("document").ready(function() {
  $('.cinema-seats .seat').click(function() {
  $(this).toggleClass('active');
});

var clk = 0;
var cnt = 0;
var ido = "";
$('[data-toggle="tooltip"]').tooltip();

$("#frmchair").submit(function(e){
e.preventDefault();

var z = $("#frmchair").serialize();
console.log(z);
});

$("td").click(function(){
var use = $(this).attr("class");

if(cnt == 0){
if(use != 'use'){
$(this).css("background","var(--based)");
ido = $(this).attr("id");
cnt = 1;
}
} else{
if(use != 'use'){
$("td[id="+ido+"]").css("background","#eee");
$(this).css("background","var(--based)");
ido = $(this).attr("id");
cnt++;
}

console.log(ido);

}
console.log(cnt);
$(this).find('input').prop("checked",true);
});

$("input[type=radioz]").click(function(){
console.log($(this).attr("id"));
$(this).parent().css("background","var(--base)");
//$(this).css("background","#eee");
});
$("input[type=reset]").click(function(){
console.log("reset");
$("td:not([class=use])").css("background","#eee");
});

});
*/
</script>

<title>Tamugo!</title>
</head>
<body>
  <div class="header">
    <nav class="navbar navbar-light bg-light shadow-sm">
      <a class="navbar-brand"><img src="img/icon.png" width="100px"></a>
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0 base" type="submit">Search</button>
      </form>
    </nav>
  </div>
  <div class="shadow-sm w-80 center">
    <div class="row contain">
      <div class="col-lg-12">
        <div class="row bs-wizard" style="border-bottom:0;">

          <div class="col-xs-3 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum">Langkah 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Konfirmasi Kehadiran</div>
          </div>

          <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
            <div class="text-center bs-wizard-stepnum"><b>Langkah 2</b></div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center textactive">Pilih Kursi / Tempat Duduk Anda</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Langkah 3</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cek Ulang Data</div>
          </div>

          <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Step 4</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Cetak Kode QR</div>
          </div>

        </div>
      </div>
      <div class="col-lg-12">
        <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
          <div class="col-lg-12">
            <h4>Pilih Tempat Duduk</h4>
            <canvas id="canvas" style="border: 1px solid #ccc;" data-toggle="tooltip"></canvas>
<!--
            <input type="submit" name="submit" id="btnsave" class="btnck">
            <input type="reset" name="reset" id="btnreset" class="btnck"> -->
          </div>
        </div>
        <!--
        <div class="col-lg-4 event-info">
        <h4>Event Info</h4>
        <img src="img/wedding-couple.png" width="200px">
        <div class="row row-event">
        <div class="col-lg-2"><ion-icon name="pin"></ion-icon></div>
        <div class="col-lg-10">Keyongan RT 02/06 Nogosari Boyolali</div>
      </div>
      <div class="row">
      <div class="col-lg-2"><ion-icon name="time"></ion-icon></div>
      <div class="col-lg-10">Agt 20, 08:00 AM</div>
    </div>
  </div>

<div class="row">
  <div class="col-lg-12">

  </div>
</div>
-->
</div>
</form>
<div class="col-lg-12">
  <div class="row" style="padding: 20px 20px;margin-top: 2rem;">
    <div class="col-lg-12">
      <a href="?page=cancel">
      <button type="button" name="button" class="btnck float-left">Batal</button>
      </a>
      <form action="" method="post">
        <input type="hidden" name="_seatid" value="" id="seatid">
        <button type="submit" name="button" class="btnext float-right">Next</button>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="canvas/js/fabric.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="package/sweetalert2/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
$("document").ready(function(){
  var canvas = new fabric.Canvas('canvas');
  var cs = $("#canvas");

  var json = '';

  $.ajax({
    url: 'API/front/canvas/<?php echo $kode; ?>',
    type: 'GET',
    success:function(res){
      json = JSON.stringify(res);
    },
    error:function(x,y,z){
      console.log("error occured !");
    },
    async: false
  });

  function resizeCanvas() {
    var bwidth = cs.parent().parent().css("width");
    canvas.setHeight(window.innerHeight);
    canvas.setWidth(parseInt(bwidth)-40);
    canvas.renderAll();
  }
  window.addEventListener('resize', resizeCanvas, false);
  resizeCanvas();

  canvas.clear();
  canvas.loadFromJSON(json, function() {
    canvas.backgroundColor = '#dedede';
    canvas.renderAll();
  });

  function exports(origin,canvas){
    let tmp = JSON.parse(origin);
    let active = tmp.active;
    let object = {};

    object["version"] = tmp.version;
    object["active"] = active;
    object["objects"] = canvas.getObjects();

    return object;
  }
  function update(canvas){
    var tmp = canvas.toJSON();

    canvas.clear();

    canvas.loadFromJSON(tmp, function() {
      canvas.renderAll();
    });
  }

  canvas.on('object:scaling',function(e){
    //e.lockUniScaling = true;
  });
  canvas.on('object:moving', function(e) {
  });

  var x=0;
  var active = JSON.parse(json).active;
  var ini;
  var oldfill;
  var state;

  canvas.on('mouse:down', function(e) {
    // var objects = canvas.getObjects();
    // for(o in objects){
    //   objects[0].set("oldfill",objects[0].fill);
    // }
    //console.log(canvas.getObjects());

    if(e.target !== null){
      if(e.target.used == false){
        // if(x < 1){
        //   ini = canvas.getActiveObject();
        //
        //   ini.set("fill",active);
        //   ini.set("used",true);
        //   ini.set("insession",true);
        //   x++;
        // } else{
        //   console.log(ini);
        //   ini.set("fill",ini.oldfill);
        //   ini.set("used",false);
        //   ini.set("insession",false);
        //
        //   var now = canvas.getActiveObject();
        //   now.set("fill",active);
        //   now.set("used",true);
        //   now.set("insession",true);
        //
        //   ini = canvas.getActiveObject();
        //   x++;
        // }

        if(x < 1){

          ini = canvas.getActiveObject();
          oldfill = ini.fill;

          ini.set("oldfill",ini.fill);

          ini.set("fill",active);
          ini.set("used",true);
          ini.set("insession",true);

          $("#seatid").val(ini.id_seat);
          x++;
          // console.log("x<=1 x",x);
        } else{
          // console.log(ini.oldfill);

          ini.set("used",false);
          ini.set("fill",oldfill);
          ini.set("insession",true);

          ini = canvas.getActiveObject();
          oldfill = ini.fill;

          var now = canvas.getActiveObject();
          now.set("fill",active);
          now.set("used",true);
          now.set("insession",true);

          $("#seatid").val(now.id_seat);
          // ini = canvas.getActiveObject();
          x++;

          // console.log("x",x);
        }
      } else{
        // console.log(e);
        // console.log("used");
        if(e.target.booked == true){
          swal({
            title: 'Info!',
            html: 'Kursi ini sudah dipesan oleh <a style="font-weight: bold">'+e.target.bookedby+'</a>',
            type: 'info',
            confirmButtonText: 'Oke'
          })
        }
      }
    }
  });
  canvas.on('mouse:over',function(e){

    if(e.target){
      if(e.target.booked !== false){

      }
    }
  });
  $("#btnsave").click(function(){
    console.log(canvas.getActiveObjects());
  });
  $("#btnreset").click(function(){

    var now = canvas.getActiveObject();
    now.set("fill",oldfill);
    now.set("used",false);
    now.set("insession",false);
    canvas.renderAll();

    // console.log(canvas.getObjects());
  });

  canvas.selection = false;
});
</script>
</body>
</html>
