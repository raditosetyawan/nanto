<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/back/cek', function (Request $request, Response $response) {
  $response->getBody()->write("Hello, back cek code");
  return $response;
});

$app->get('/back/canvas/{id}', function (Request $request, Response $response,array $args) use($api){
  $args = (object) $args;

  $id = $args->id;
  $z = $api->getCanvasByID($id);

  $body = $response->withJson($z,200,JSON_PRETTY_PRINT);

  return $body;
});

$app->post('/back/canvas',function(Request $request,Response $response) use($api){
  $args = json_encode($request->getParsedBody());

  $log = $api->saveCanvas($args);

  if($log){
    $data = [
      "status" => "success",
      "error" => "null",
      "message" => "Canvas Saved"
    ];
  } else{
    $data = [
      "status" => "error",
      "error" => "error occured"
    ];
  }

  $new = $response->withJson($data,200);

  //$new = $response->getBody()->write(var_dump($log));

  return $new;
});

$app->get('/back/canvas2/{kode}', function (Request $request, Response $response) use($api,$res_ok,$res_error){
  $req = (array) $request->getParsedBody();
  $kode = $request->getAttribute("kode");

  $data = $api->getDataFront($kode);
  $json = $response->withJson($data,200,JSON_NUMERIC_CHECK);

  return $json;
});

$app->put('/back/canvas', function (Request $request, Response $response) use($api,$res_ok,$res_error,$app){
  $req = (array) $request->getParsedBody();
  $contentType = $request->getContentType();

  if (strpos($contentType, 'application/json') !== 0) {
    $res_error['message'] = "Content type not json!";

    return $response->withJson($res_error,400);
  }
  if(!isset($req['active'])){
    $res_error['message'] = "Json parameter error!";
    return $response->withJson($res_error,400);
  }

  if(!isset($req['objects'])){
    $res_error['message'] = "Json parameter error!";
    return $response->withJson($res_error,400);
  }
  if(!isset($req['kode'])){
    $res_error['message'] = "Json parameter error!";
    return $response->withJson($res_error,400);
  }
  if(!isset($req['deleted'])){
    $res_error['message'] = "Json parameter error!";
    return $response->withJson($res_error,400);
  }


  if($api->updateCanvas($req)){
    $res_ok["message"] = "Canvas successfully updated!";

    return $response->withJson($res_ok,200);
  }

  return false;
});
?>
