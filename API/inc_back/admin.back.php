<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/back/admin', function (Request $request, Response $response,array $args) use($data){
  $dataz = $request->getParsedBody();

  $re = $data->insertAdmin($dataz);

  if($re){
    $dres = [
      "status" => "success",
      "error" => "false",
      "msg" => "Data Tersimpan"
    ];
  } else{
    $dres = [
      "status" => "error",
      "error" => "true",
      "msg" => "Error Occured"
    ];
  }

  $new = $response->withJson($dres,200,JSON_PRETTY_PRINT);
  return $new;
});

$app->delete('/back/admin/{id}', function (Request $request, Response $response,array $args) use($data){
  $id = $args['id'];

  $re = $data->delAdmin($id);

  if($re){
    $dres = [
      "status" => "success",
      "error" => "false",
      "msg" => "Data Terhapus"
    ];
  } else{
    $dres = [
      "status" => "error",
      "error" => "true",
      "msg" => "Error Occured"
    ];
  }

  $new = $response->withJson($dres,200,JSON_PRETTY_PRINT);
  return $new;
});

?>
