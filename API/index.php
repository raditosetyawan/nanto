<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\App;
use dbase\datafunction as data;
use dbase\apifunction as API;

require '../vendor/autoload.php';
require '../lib/helper.php';

session_start();

$app = new App(['settings' => [
  'displayErrorDetails' => true
]
]);

$data = new data();
$api = new API();

$res_error = [
  "status" => "error",
  "message" => ""
];

$res_ok = [
  "status" => "success",
  "message" => ""
];

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
  $name = $args['name'];
  $response->getBody()->write("Hello, $name");

  return $response;
});

require_once "inc_front/cek.front.php";
require_once "inc_front/get.front.php";

if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
  require_once "inc_back/canvas.back.php";
  require_once "inc_back/admin.back.php";
}

$app->run();
?>
