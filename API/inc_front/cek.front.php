<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/front/cek', function (Request $request, Response $response){
    $response->getBody()->write("Hello, front cek code");
    return $response;
});

$app->post('/front/cek', function (Request $request, Response $response) use($api,$res_ok,$res_error){
    $req = (array) $request->getParsedBody();

    if($api->chkUndangan($req)){
        if($api->chkIfConfirmed($req)){
            $res_ok['status'] = "exist";
            $res_ok['message'] = "user sudah konfirmasi";

            return $response->withJson($res_ok,200);
        }

        $res_ok['message'] = "data ada di database";
        $json = $response->withJson($res_ok,200);

    } else{
        $res_error['message'] = "data tidak ada di database";
        $json = $response->withJson($res_error,400);
    }

    return $json;
});

?>
