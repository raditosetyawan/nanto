<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

if(isset($_SESSION['client']['prebook']) && isset($_SESSION['client']['kode']) && isset($_SESSION['client']['nama']) && isset($_SESSION['client']['email']) && isset($_SESSION['client']['step'])){

  $app->get('/front/canvas/{kode}', function (Request $request, Response $response) use($api,$res_ok,$res_error){
      $req = (array) $request->getParsedBody();
      $kode = $request->getAttribute("kode");

      if($_SESSION['client']['kode'] != $kode){
        $res_error['message'] = "event code not match with session";

        return $response->withJson($res_error,200);
      }

      $data = $api->getDataFront($kode);
      $json = $response->withJson($data,200,JSON_NUMERIC_CHECK);

      return $json;
  });

  $app->post('/front/canvas/save',function(Request $request,Response $response) use ($api,$res_ok,$res_error){
    $req = $request->getParsedBody();

    return false;
  });
}

?>
